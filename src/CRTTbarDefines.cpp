#include <random>
#include <chrono>
#include "CRTTbarDefines.h"

namespace CRTTbar
{
  uint32_t tag_fatjet_idx(const ROOT::VecOps::RVec<TLorentzVector>& muon_p4, const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4)
  {
    const TLorentzVector& crttbar_muon = muon_p4[0];
    float min_dR = 0.04 + 10.0/crttbar_muon.Pt();

    // Loop over fatjets, find closest one with leading btag and pT > 250GeV
    for (uint32_t i = 0; i < std::min<uint32_t>(2, fatjet_p4.size()); ++i)
      {
	// Same dR requirements (just in case)
	float dR = crttbar_muon.DeltaR(fatjet_p4[i]);
	// to get closest fatjet
	if (dR > min_dR && dR < 1.5)
	  return i;
      }
    return 666;
  }

  uint32_t probe_fatjet_idx(uint32_t tag_fatjet_idx)
  {
    return (tag_fatjet_idx+1)%2;
  }

  // returns whether the opposite hemisphere requirement of probe and tag jet is satisfied
  bool filter_probe_tag_deltaPhi(uint32_t tag_fatjet_idx, uint32_t probe_fatjet_idx, const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4)
  {
    const TLorentzVector& tag_jet   = fatjet_p4[tag_fatjet_idx  ];
    const TLorentzVector& probe_jet = fatjet_p4[probe_fatjet_idx];

    float dPhi = fabs(tag_jet.DeltaPhi(probe_jet));
    return dPhi > 2*M_PI/3;
  }

  std::vector<uint32_t> tag_vrjets(uint32_t tag_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<float>& trkjet_pt, const ROOT::VecOps::RVec<float>& trkjet_R, const ROOT::VecOps::RVec<float>& trkjet_muon_dR)
  {
    const std::vector<uint32_t>& in=fatjet_trkjet_idxs[tag_fatjet_idx];
    std::vector<uint32_t> out;
    std::copy_if(in.begin(), in.end(), std::back_inserter(out), [&trkjet_pt, &trkjet_R, &trkjet_muon_dR](uint32_t tidx) { return trkjet_pt[tidx]>10 && trkjet_muon_dR[tidx]/trkjet_R[tidx]>0.2; } );
    return out;
  }

  std::vector<uint32_t> probe_vrjets(uint32_t probe_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<float>& trkjet_pt)
  {
    const std::vector<uint32_t>& in=fatjet_trkjet_idxs[probe_fatjet_idx];
    std::vector<uint32_t> out;
    std::copy_if(in.begin(), in.end(), std::back_inserter(out), [&trkjet_pt](uint32_t tidx) { return trkjet_pt[tidx]>10; } );
    return out;
  }

  // returns number of b-tags in tag jet
  uint32_t tagjet_nbtag(const std::vector<uint32_t>& tagjet_trkjet_idx, const ROOT::VecOps::RVec<bool>& trkjet_btag)
  {
    // Loop over btags, up to 1 of them.
    uint32_t n_bjets = 0;
    for (uint32_t j = 0; j < std::min<int>(2, tagjet_trkjet_idx.size()); ++j)
      n_bjets += trkjet_btag[tagjet_trkjet_idx[j]];

    return n_bjets;
  }

  uint32_t probejet_nbtag(const std::vector<uint32_t>& probejet_trkjet_idx, const ROOT::VecOps::RVec<bool>& trkjet_btag)
  {
    // Loop over btags, up to 3 of them
    uint32_t n_bjets = 0;
    for (uint32_t j = 0; j < std::min<int>(3, probejet_trkjet_idx.size()); ++j)
      n_bjets += trkjet_btag[probejet_trkjet_idx[j]];

    return n_bjets;
  }

  // returns SF from b-tags in tag jet
  double btagSF(const std::vector<uint32_t>& tagjet_trkjet_idx, const std::vector<uint32_t>& probejet_trkjet_idx, const std::vector<std::vector<float>>& trkjet_SF, uint32_t trkjet_SF_idx)
  {
    double SF = 1.0;
    // Get SF from leading btagged vrJet in tagJet
    for (uint32_t j = 0; j < std::min<int>(2, tagjet_trkjet_idx.size()); ++j)
      SF *= trkjet_SF[tagjet_trkjet_idx[j]][trkjet_SF_idx];
    // Get SF(s) from trkjets in probe fatjet
    for (uint32_t j = 0; j < std::min<int>(3, probejet_trkjet_idx.size()); ++j)
      SF *= trkjet_SF[probejet_trkjet_idx[j]][trkjet_SF_idx];

    return SF;
  }

  // return new weight for MC based on polynomial fit between srs and crttbar in pt0
  float get_poly_reweight(const std::vector<float>& fatjet_pt, const uint32_t idx)
  {
    float weight = 1.0;
	float pt = fatjet_pt[idx];
	// if in pt1 or pt2, keep weight at 1
	if (pt > 450) return weight;
	weight = 3.11067 - 0.0170995 * pt + 2.79471e-05 * pt*pt;
	// weight cannot be greater than 1
	if (weight > 1) weight = 1;
	return weight;
  }
  
  // toss data events using the accept/reject method
  bool random_toss(const std::vector<float>& fatjet_pt, const uint32_t idx)
  {
      float pt = fatjet_pt[idx];
      // if in pt1 or pt2, keep event
      if (pt >= 450) return true;
      float p = 3.11067 - 0.0170995 * pt + 2.79471e-05 * pt*pt;
      // p cannot be greater than 1.
      if (p > 1) p = 1;
      // random number between 1 and 0 (uniform dist)
      // rand gen
      std::mt19937_64 rng;
      // initialize the random number generator with time-dependent seed
      uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
      std::seed_seq ss{uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed>>32)};
      rng.seed(ss);
      // initialize uniform dist between 0 and 1
      std::uniform_real_distribution<float> unif(0.0, 1.0);
      // get random number
      float r = unif(rng);
      // if r < p, keep, otherwise toss
      if (r < p) return true;
      else return false;
  }

}
