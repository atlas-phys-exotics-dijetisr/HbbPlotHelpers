#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <getopt.h>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "TruthDijetAnalysis.h"

// -- Options --
uint32_t nthreads=0;

void usage(char* argv[])
{ 
  std::cerr << "Usage: " << argv[0] << " [options] outDir [sample [selection]]" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -t, --threads  Number of threads (default: " << nthreads << ")" << std::endl;
  std::cerr << "" << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
        {
          {"threads", required_argument, 0,  't' },
          {0,         0,              0, 0       }
        };

      c = getopt_long(argc, argv, "t:", long_options, &option_index);
      if (c == -1)
        break;

      switch (c)
        {
        case 't':
          nthreads = std::stoi(optarg);
          break;
        default:
          std::cerr << "Invalid option supplied. Aborting." << std::endl;
          std::cerr << std::endl;
          usage(argv);
          return 1;
        }
    }

  if (optind == argc)
    {
      std::cerr << "Required outDir argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  // Parse arguments
  if(argc<3)
    {
      std::cerr << "usage: " << argv[0] << " outDir [sample [selection]]" << std::endl;
      return 1;
    }
  std::string outDir=argv[optind++];
  std::vector<std::string> saves={};
  std::vector<std::string> selections={"bbl","bbs","bxl","bxs","xxl","xxs"};

  if(optind<argc)
    saves={argv[optind++]};

  if(optind<argc)
    {
      std::stringstream ss(argv[optind++]);
      selections.clear();
      std::string selection;
      while(std::getline(ss,selection,','))
        selections.push_back(selection);
    }

  ROOT::EnableImplicitMT(nthreads);

  // Process stuff
  SampleFactory sf("../truthsamples.yml");

  // TH1 Histogram list
  std::vector<ROOT::RDF::TH1DModel> hists;
  hists.push_back({"Hcand_m"       ,"", 44,60, 280});
  hists.push_back({"Hcand_pt"      ,"",95,250,1200});

  // TH2 Histogram list
  std::vector<ROOT::RDF::TH2DModel> hists2D;
  hists2D.push_back({"Corr_m_pt"   ,"",44,60,280,95,250,1200});

  std::map<std::string, std::shared_ptr<ROOT::RDF::RNode> TruthDijetAnalysis::*> selectionMap; 
  selectionMap["bbl"]=&TruthDijetAnalysis::df_bbl_inc;
  selectionMap["bbs"]=&TruthDijetAnalysis::df_bbs_inc;
  selectionMap["bxl"]=&TruthDijetAnalysis::df_bxl_inc;
  selectionMap["bxs"]=&TruthDijetAnalysis::df_bxs_inc;
  selectionMap["xxl"]=&TruthDijetAnalysis::df_xxl_inc;
  selectionMap["xxs"]=&TruthDijetAnalysis::df_xxs_inc;
  
  std::vector<std::string> cachelist={"w"}; // Cache branches for each selection
  std::transform(hists.begin(),hists.end(),std::back_inserter(cachelist),
                 [](const ROOT::RDF::TH1DModel& histDef) { return histDef.fName.Data(); });

  // Analysis
  ROOT::RDF::RResultPtr<TH1D> h;
  ROOT::RDF::RResultPtr<TH2D> h2D;

  for(const std::string& save : saves)
    {
      std::cout << "Processing " << save << std::endl;
      if(!sf.contains(save))
        {
          std::cerr << "WARNING: Unable to find \"" << save << "\"! Skip..." << std::endl;
          continue;
        }

      std::shared_ptr<ROOT::RDF::RNode> df=sf[save]->dataframe();
      uint32_t nevents=df->Count().GetValue();
      TruthDijetAnalysis a(df,sf[save]->isMC());

      // Loop the selection loop!
      std::string outFile=outDir+"/"+save+".root";
      TFile *fh_out=TFile::Open(outFile.c_str(),"RECREATE");
      for(const std::pair<std::string, std::shared_ptr<ROOT::RDF::RNode> TruthDijetAnalysis::*> selection : selectionMap)
        {
          fh_out->mkdir(selection.first.c_str());
          fh_out->cd   (selection.first.c_str());

          ROOT::RDF::RNode cache=(a.*(selection.second))->Cache(cachelist);
          for(const ROOT::RDF::TH1DModel& hdef : hists)
            {
              h=cache.Histo1D(hdef,hdef.fName.Data(),"w");
              std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
              h->Write(hdef.fName);
              std::chrono::steady_clock::time_point end   = std::chrono::steady_clock::now();
              std::cout << "\t\t" << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms" << std::endl;
            }
          for(const ROOT::RDF::TH2DModel& hdef : hists2D)
            { 
              h2D = cache.Histo2D(hdef,"Hcand_m","Hcand_pt","w");
              std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
              h2D->Write(hdef.fName); 
              std::chrono::steady_clock::time_point end   = std::chrono::steady_clock::now();
              std::cout << "\t\t" << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms" << std::endl;
            }
        }
      fh_out->Close();
    }
}
