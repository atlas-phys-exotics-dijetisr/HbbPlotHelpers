#include "WeightDB.h"

#include <fstream>
#include <iostream>

const std::unordered_map<uint32_t, uint32_t> WeightDB::empty;

WeightDB::WeightDB()
{ }

void WeightDB::scanFile(uint32_t dsid, const std::string& path)
{
  std::ifstream fh(path);
  std::string line;
  uint32_t idx=0;
  while(std::getline(fh,line))
    m_db[line][dsid]=idx++;
}

uint32_t WeightDB::systIdx(uint32_t dsid, const std::string& systName) const
{
  return m_db.at(systName).at(dsid);
}

const std::unordered_map<uint32_t, uint32_t>& WeightDB::systIdx(const std::string& systName) const
{
  std::unordered_map<std::string, std::unordered_map<uint32_t, uint32_t>>::const_iterator entry=m_db.find(systName);
  if(entry==m_db.end()) return empty;
  return entry->second;
}


