#include "SampleFilelist.h"

#include <TFile.h>
#include <TAxis.h>
#include <ROOT/TThreadExecutor.hxx>

#include <iostream>

SampleFilelist::SampleFilelist(const std::vector<std::string>& filelist, bool isMC)
  : Sample(),m_isMC(isMC),m_filelist(filelist)
{ }

SampleFilelist::SampleFilelist(bool isMC)
  : Sample(),m_isMC(isMC)
{ }

void SampleFilelist::addFile(const std::string& path)
{
  m_filelist.push_back(path);
}

void SampleFilelist::addFilelist(const std::string& path)
{
  std::ifstream fh_fl(path);

  std::string line;
  while(std::getline(fh_fl,line))
    {
      if(line=="") continue;
      addFile(line);
    }
}

bool SampleFilelist::isMC() const
{ return m_isMC; }

void SampleFilelist::setWeighted(bool weighted)
{ m_weighted=weighted; }

bool SampleFilelist::weighted() const
{ return m_weighted; }

std::vector<std::string> SampleFilelist::filelist()
{
  init_meta();

  return m_filelist;
}

std::unordered_map<uint32_t, double> SampleFilelist::scales()
{
  init_meta();

  std::unordered_map<uint32_t, double> theScales=m_norm;
  for(const std::pair<uint32_t, double>& kv : m_norm)
    theScales[kv.first]=m_scale/kv.second;

  return theScales;
}

std::unordered_map<uint32_t, std::vector<double>> SampleFilelist::sumW()
{
  init_meta();

  return m_sumw;
}

std::unordered_map<uint32_t, uint32_t> SampleFilelist::weightIdx()
{
  init_meta();

  std::unordered_map<uint32_t, uint32_t> theWeightIdx;
  for(const std::pair<uint32_t, double>& kv : m_norm)
    theWeightIdx[kv.first]=(m_weightIdxSet)?m_weightIdx:0;

  return theWeightIdx;
}

std::unordered_map<uint32_t, std::vector<uint32_t>> SampleFilelist::extraWeights()
{
  init_meta();

  std::unordered_map<uint32_t, std::vector<uint32_t>> theExtraWeights;
  for(const std::pair<uint32_t, double>& kv : m_norm)
    {
      if(m_extraWeightsSet)
	theExtraWeights[kv.first]=m_extraWeights;
      else
	theExtraWeights[kv.first]={};
    }

  return theExtraWeights;
}

void SampleFilelist::init_meta()
{
  if(m_isMetaLoaded) return;

  //
  // Load all of the metadata

  // to capture
  bool mc=isMC();
  bool weighted=m_weighted;

  struct SampleMetaCounts
  {
    bool good=false;
    std::unordered_map<uint32_t, double             > norm;
    std::unordered_map<uint32_t, std::vector<double>> sumw;
  };

  ROOT::TThreadExecutor pool;
  std::vector<SampleMetaCounts> metaCounts=pool.Map([&mc,&weighted](const std::string& path)->SampleMetaCounts
						    {
						      SampleMetaCounts result;

						      TFile *fh=TFile::Open(path.c_str());
						      TTree *t=fh->Get<TTree>("outTree");
						      result.good=(t!=nullptr);

						      // MC normalizations
						      if(mc && result.good)
							{
							  // Determine what is in the sample
							  int32_t mcChannelNumber=0;
							  int32_t runNumber      =0;							  
							  t->SetBranchStatus ("*"              ,0);
							  t->SetBranchStatus ("runNumber"      ,1);
							  t->SetBranchStatus ("mcChannelNumber",1);
							  t->SetBranchAddress("runNumber"      ,&runNumber);
							  t->SetBranchAddress("mcChannelNumber",&mcChannelNumber);
							  t->GetEntry(0);

							  // Get normalization
							  uint32_t dskey=runNumber*1000+mcChannelNumber;
							  TH1D *h_EventCount=fh->Get<TH1D>("MetaData_EventCount");
							  if(h_EventCount==nullptr) h_EventCount=fh->Get<TH1D>("MetaData_EventCount_Test");
							  if(h_EventCount==nullptr)
							    throw std::string("Input file "+path+" is missing meta data!");

							  result.norm[dskey] =(weighted)?h_EventCount->GetBinContent(3):h_EventCount->GetBinContent(1);

							  // Get alternate normalizations
							  TH1D *h_SumW=fh->Get<TH1D>("MetaData_SumW");
							  if(h_SumW!=nullptr)
							    {
							      result.sumw[dskey]={ h_EventCount->GetBinContent(3) }; // position 0 is nominal weight
							      std::vector<double>& this_sumw=result.sumw[dskey];

							      for(uint32_t binidx=1; binidx<h_SumW->GetNbinsX()+1; binidx++)
								{
								  if(h_SumW->GetBinContent(binidx)==0) break;
								  uint32_t widx=binidx-1;

								  if(widx>=this_sumw.size()) this_sumw.resize(widx+1,0.);
								  this_sumw[widx]+=h_SumW->GetBinContent(binidx);
								}
							    }
							}

						      // finish
						      fh->Close();
						      return result;
						    }, m_filelist);

  //
  // merge the results
  std::vector<std::string> goodfilelist;
  m_norm.clear();
  m_sumw.clear();
  for(uint32_t idx=0;idx<metaCounts.size();idx++)
    {
      // Check if file is OK
      if(metaCounts[idx].good) goodfilelist.push_back(m_filelist[idx]);
      // Merge normalizations
      for(const std::pair<uint32_t, double> kv : metaCounts[idx].norm)
	{
	  if(m_norm.count(kv.first)) m_norm[kv.first]+=kv.second;
	  else m_norm[kv.first]=kv.second;
	}
      // Merge alternate sumW
      for(const std::pair<uint32_t, std::vector<double>> kv : metaCounts[idx].sumw)
	{
	  if(m_sumw.count(kv.first))
	    {
	      for(uint32_t jdx=0;jdx<kv.second.size();jdx++)
		m_sumw[kv.first][jdx]+=kv.second[jdx];
	    }
	  else
	    m_sumw[kv.first]=kv.second;
	}
    }
  m_filelist=goodfilelist;

  m_isMetaLoaded=true;
}

