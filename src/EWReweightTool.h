#ifndef EWREWEIGHTTOOL_H
#define EWREWEIGHTTOOL_H

#include <string>

#include <TF1.h>
#include <TFile.h>

/**
 * Tool for applying EW reweighting to Higgs samples
 *
 * \param ewCorrections Name of the file containing the EW corrections for Higgs
 */
class EWReweightTool
{
public:
  EWReweightTool() =default;
  
  /**
   * \param ewCorrections Name of the file containing the EW corrections for Higgs
   */
  EWReweightTool(const std::string &ewCorrections);
  ~EWReweightTool();

  /** \brief Calculate the correction
   *
   * \param mcChannelNumber DSID of dataset (identifies production mode)
   * \param truth_pt pT of the Higgs particle
   * \param mcEventWeights Alternate MC weights
   *
   * \return multiplicative EW correction
   */
  float calcEWCorrection(int mcChannelNumber, float truth_pt, const std::vector<float>& mcEventWeights);

private:
  // Pointer to file
  TFile* m_fEW =nullptr;

  // Reweighting functions
  TF1* m_EW_WpH =nullptr;
  TF1* m_EW_WmH =nullptr;
  TF1* m_EW_ZllH=nullptr;
  TF1* m_EW_ZvvH=nullptr;
  TF1* m_EW_VH  =nullptr;
  TF1* m_EW_VBF =nullptr;
  TF1* m_EW_ttH =nullptr;
};

#endif // EWREWEIGHTTOOL_H
