#ifndef DEFINES_H
#define DEFINES_H

#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <TH2.h>

#include <unordered_map>

#define EF_prescaled (0x1 << 2)

const std::unordered_map<std::string, std::vector<std::string>> btagging_systematics={
  { "MV2c10_FixedCutBEff_77", {"",
			       "FT_EFF_Eigen_B_0__1down","FT_EFF_Eigen_B_0__1up","FT_EFF_Eigen_B_1__1down","FT_EFF_Eigen_B_1__1up","FT_EFF_Eigen_B_2__1down","FT_EFF_Eigen_B_2__1up","FT_EFF_Eigen_B_3__1down","FT_EFF_Eigen_B_3__1up","FT_EFF_Eigen_B_4__1down","FT_EFF_Eigen_B_4__1up",
			       "FT_EFF_Eigen_C_0__1down","FT_EFF_Eigen_C_0__1up","FT_EFF_Eigen_C_1__1down","FT_EFF_Eigen_C_1__1up","FT_EFF_Eigen_C_2__1down","FT_EFF_Eigen_C_2__1up","FT_EFF_Eigen_C_3__1down","FT_EFF_Eigen_C_3__1up",
			       "FT_EFF_Eigen_Light_0__1down","FT_EFF_Eigen_Light_0__1up","FT_EFF_Eigen_Light_1__1down","FT_EFF_Eigen_Light_1__1up","FT_EFF_Eigen_Light_2__1down","FT_EFF_Eigen_Light_2__1up","FT_EFF_Eigen_Light_3__1down","FT_EFF_Eigen_Light_3__1up","FT_EFF_Eigen_Light_4__1down","FT_EFF_Eigen_Light_4__1up",
			       "FT_EFF_extrapolation__1down","FT_EFF_extrapolation__1up","FT_EFF_extrapolation_from_charm__1down","FT_EFF_extrapolation_from_charm__1up"}},
  { "DL1_FixedCutBEff_77", {"",
			    "FT_EFF_Eigen_B_0__1down","FT_EFF_Eigen_B_0__1up","FT_EFF_Eigen_B_1__1down","FT_EFF_Eigen_B_1__1up","FT_EFF_Eigen_B_2__1down","FT_EFF_Eigen_B_2__1up","FT_EFF_Eigen_B_3__1down","FT_EFF_Eigen_B_3__1up","FT_EFF_Eigen_B_4__1down","FT_EFF_Eigen_B_4__1up",
			    "FT_EFF_Eigen_C_0__1down","FT_EFF_Eigen_C_0__1up","FT_EFF_Eigen_C_1__1down","FT_EFF_Eigen_C_1__1up","FT_EFF_Eigen_C_2__1down","FT_EFF_Eigen_C_2__1up","FT_EFF_Eigen_C_3__1down","FT_EFF_Eigen_C_3__1up",
			    "FT_EFF_Eigen_Light_0__1down","FT_EFF_Eigen_Light_0__1up","FT_EFF_Eigen_Light_1__1down","FT_EFF_Eigen_Light_1__1up","FT_EFF_Eigen_Light_2__1down","FT_EFF_Eigen_Light_2__1up","FT_EFF_Eigen_Light_3__1down","FT_EFF_Eigen_Light_3__1up","FT_EFF_Eigen_Light_4__1down","FT_EFF_Eigen_Light_4__1up","FT_EFF_Eigen_Light_5__1down","FT_EFF_Eigen_Light_5__1up",
			    "FT_EFF_extrapolation__1down","FT_EFF_extrapolation__1up","FT_EFF_extrapolation_from_charm__1down","FT_EFF_extrapolation_from_charm__1up"}}
};

ROOT::VecOps::RVec<TLorentzVector> PtEtaPhiM(int32_t n, const std::vector<float>& pt, const std::vector<float>& eta, const std::vector<float>& phi, const std::vector<float>& m);
ROOT::VecOps::RVec<TLorentzVector> PtEtaPhiE(int32_t n, const std::vector<float>& pt, const std::vector<float>& eta, const std::vector<float>& phi, const std::vector<float>& e);

std::vector<float> GetMass(const ROOT::VecOps::RVec<TLorentzVector>&);

//! Match an object to another one in a list using dR
/**
 * Match is defined as the object with the smallest dR
 *
 * \param pt pT of the object
 * \param eta Eta of the object
 * \param phi phi of the object
 * \param m mass of the object
 * \param truthjet_p4 list of four-vectors to match against
 *
 * \return index of the closest truthjet_p4
 */
uint32_t matchdR_idx(float pt, float eta, float phi, float m, const ROOT::VecOps::RVec<TLorentzVector>& truthjet_p4);

//! Calculate reweighting factor using a 2D histogram
/**
 * Input values falling outside of the histogram range are
 * mapped into the first/last bin.
 *
 * \param hMap map to look up
 * \param truth_m the x-axis lookup value
 * \param truth_pt the y-axis lookup value
 *
 * \return reweighting value for (truth_m, truth_pt)
 */
float getReweight(TH2* hMap, float truth_m, float truth_pt);

//! Count number of truth hadrons associated to antikt10 jets 
/**
 * \param jet_p4 vector of antikt04 jets
 * \param fatjet_p4 vector of antikt10 jets
 * \param jet_GhostHadronsFinalCount vector containing the 
 *        number of truht hadrons of a certain type inside
 *        each antikt04 jet
 *
 * \return vector containing the number of hadrons of a 
 *         certain type inside each antikt10 jet
 */
std::vector<int> CountHadrons(const ROOT::VecOps::RVec<TLorentzVector>& jet_p4,
                              const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4,
                              const ROOT::VecOps::RVec<int>& jet_GhostHadronsFinalCount);

bool filter_flag(bool flag);

bool filter_or(bool column0, bool column1, bool column2, bool column3);

bool filter_trigger_or1(int8_t trigger0);
bool filter_trigger_or4(int8_t trigger0, int8_t trigger1, int8_t trigger2, int8_t trigger3);

bool trigger_jet_ptmass(const std::vector<float>& fatjet_pt, const std::vector<float>& fatjet_m, float ptcut, float mcut);

/**
 * Return property of a particle for a variable index
 *
 * \tparam vartype the variable type
 *
 * \param idx index for the particle of interest
 * \param var list of all all object properties of interest
 *
 * \return Property var[idx]
 */
template<class vartype>
vartype define_idx(const std::vector<vartype>& var, uint32_t idx)
{ return var.at(idx); }

/**
 * Return property of a particle for a fixed index
 *
 * \tparam vartype the variable type
 * \tparam idx index for the particle of interest
 *
 * \param var list of all all object properties of interest
 *
 * \return Property var[idx]
 */
template<class vartype, uint32_t idx>
vartype define(const std::vector<vartype>& var)
{ return define_idx<vartype>(var, idx); }


float    define_float_idx  (const std::vector       <float   >& vec, uint32_t idx);
float    define_float_idx0 (const std::vector       <float   >& vec);
float    define_float_ridx (const ROOT::VecOps::RVec<float   >& vec, uint32_t idx);

uint32_t define_uint32_ridx(const ROOT::VecOps::RVec<uint32_t>& vec, uint32_t idx);

int32_t define_int32_idx (const std::vector       <int32_t>& vec, uint32_t idx);
int32_t define_int32_ridx(const ROOT::VecOps::RVec<int32_t>& vec, uint32_t idx);

//! Size of a list (ie: nParticles)
/**
 * \tparam T type of vector
 * \param list list under query
 *
 * \return Size of the list
 */
template<class T>
uint32_t vector_size(const std::vector<T>& list)
{ return list.size(); }

//!DeltaR between two objects
/**
 * Wraparound of phi correctly applied
 *
 * \param eta0 eta of particle 0
 * \param phi0 phi of particle 0
 * \param eta1 eta of particle 1
 * \param phi1 phi of particle 1
 *
 * \return sqrt((eta0-eta1)^2+(phi0-phi1)^2)
 */
float DeltaR(float eta0, float phi0, float eta1, float phi1);

//!DeltaPhi between two objects
/**
 * Between [0,pi]
 *
 * \param phi0 phi of particle 0
 * \param phi1 phi of particle 1
 *
 * \return |phi0-phi1|
 */
float DeltaPhi(float phi0, float phi1);

ROOT::VecOps::RVec<uint32_t> indexlist(int32_t n);

//
// Fat Jet functions
//

/**
 * Count the number of track jets associated to a fatjet
 *
 * \param idx Index of the fatjet
 * \param fatjet_trkjet_idxs Fat jet to track jet association map
 *
 * \return Number of track jets for fatjet at idx
 */
uint32_t fatjet_ntrkjet(uint32_t idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs);

ROOT::VecOps::RVec<float> fatjet_boost(const std::vector<float>& trkjet_pt,const std::vector<float>& trkjet_m);

/**
 * Return property of track jet assciated to a fatjet
 *
 * \tparam vartype the variable type
 *
 * \param fatjet_idx fat jet index containing track jets to check against
 * \param trkjet_idx track jet index (within fat jet) to check overlap for
 * \param fatjet_trkjet_idxs Fat jet to track jet association map
 * \param trkjet_var list of all track jet properties of interest
 *
 * \return Property var for trkjet_idx of fatjet_idx
 */
template<class vartype>
float fatjet_trkjet(uint32_t fatjet_idx, uint32_t trkjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<vartype>& trkjet_var)
{
  const std::vector<uint32_t>& fatjet_trkjet_idx=fatjet_trkjet_idxs[fatjet_idx];
  return trkjet_var[fatjet_trkjet_idx[trkjet_idx]];
}

/**
 * Return property of track jet assciated to a fatjet for a fixed fat jet
 * track jet indexes.
 *
 * \tparam vartype the variable type
 * \tparam fatjet_idx fat jet index containing track jets to check against
 * \tparam trkjet_idx track jet index (within fat jet) to check overlap for

 * \param fatjet_trkjet_idxs Fat jet to track jet association map
 * \param trkjet_var list of all track jet properties of interest
 *
 * \return Property var for trkjet_idx of fatjet_idx
 */
template<class vartype, uint32_t fatjet_idx, uint32_t trkjet_idx>
vartype fatjet_trkjet_idx(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<vartype>& trkjet_var)
{ return fatjet_trkjet<vartype>(fatjet_idx, trkjet_idx, fatjet_trkjet_idxs, trkjet_var); }

/**
 * Return property of track jet assciated to a fatjet for a fixed
 * track jet index.
 *
 * \tparam vartype the variable type
 * \tparam trkjet_idx track jet index (within fat jet) to return value for

 * \param fatjet_trkjet_idxs Fat jet to track jet association map
 * \param trkjet_var list of all track jet properties of interest
 * \param fatjet_idx fat jet index containing track jets to check against
 *
 * \return Property var for trkjet_idx of fatjet_idx
 */
template<class vartype, uint32_t trkjet_idx>
vartype fatjet_trkjet_tjidx(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<vartype>& trkjet_var, uint32_t fatjet_idx)
{ return fatjet_trkjet<vartype>(fatjet_idx, trkjet_idx, fatjet_trkjet_idxs, trkjet_var); }

/**
 * Determines whether a track jet passes the FTAG overlap criteria.
 *
 * Overlap is checked with all track jets associated to a fatjet.
 *
 * \param fatjet_idx fat jet index containing track jets to check against
 * \param trkjet_idx track jet index (within fat jet) to check overlap for
 * \param fatjet_trkjet_idxs Fat jet to track jet association map
 * \param trkjet_pt list of all track jet pT's
 * \param trkjet_eta list of all track jet eta's
 * \param trkjet_phi list of all track jet phi's
 *
 * \return True if the track jet overlaps, false otherwise.
 */
bool fatjet_trkjet_olap(uint32_t fatjet_idx, uint32_t trkjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs,
			const std::vector<float>& trkjet_pt, const std::vector<float>& trkjet_eta, const std::vector<float>& trkjet_phi);

/**
 * Determines whether a track jet passes the FTAG overlap criteria for
 * a given fat jet and track jet indexes.
 *
 * \tparam fatjet_idx fat jet index containing track jets to check against
 * \tparam trkjet_idx track jet index (within fat jet) to check overlap for
 *
 * \param fatjet_trkjet_idxs Fat jet to track jet association map
 * \param trkjet_pt list of all track jet pT's
 * \param trkjet_eta list of all track jet eta's
 * \param trkjet_phi list of all track jet phi's
 *
 * \return True if the track jet overlaps, false otherwise.
 */
template<uint32_t fatjet_idx, uint32_t trkjet_idx>
bool fatjet_trkjet_olap_idx(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs,
			    const std::vector<float>& trkjet_pt, const std::vector<float>& trkjet_eta, const std::vector<float>& trkjet_phi)
{ return fatjet_trkjet_olap(fatjet_idx, trkjet_idx, fatjet_trkjet_idxs, trkjet_pt, trkjet_eta, trkjet_phi); }


ROOT::VecOps::RVec<float> trackjet_VR(const std::vector<float>& trkjet_pt);
ROOT::VecOps::RVec<bool> fatjet_VR_overlap(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);
ROOT::VecOps::RVec<bool> fatjet_VR_overlap2(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);
ROOT::VecOps::RVec<bool> fatjet_VR_overlap3(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);
ROOT::VecOps::RVec<bool> fatjet_VR_overlapN(uint32_t N, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);

std::vector<std::vector<uint32_t>> fatjet_trkjet5_idxs (const std::vector<std::vector<uint32_t>>& idxs, const std::vector<float>& trkjet_pt);
std::vector<std::vector<uint32_t>> fatjet_trkjet10_idxs(const std::vector<std::vector<uint32_t>>& idxs, const std::vector<float>& trkjet_pt);

//! Count number of subjets
/**
 * \param idxs fat jet to track jet association
 *
 * \return list with elements being the number of track jets in the fat jet
 */
ROOT::VecOps::RVec<uint32_t> count_subjet   (const std::vector<std::vector<uint32_t>>& idxs);

ROOT::VecOps::RVec<uint32_t> count_bjetsin2 (const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int>& trkjet_btag);
ROOT::VecOps::RVec<uint32_t> count_bjetsin3 (const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int>& trkjet_btag);
ROOT::VecOps::RVec<uint32_t> count_bjetsinN (uint32_t N, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int>& trkjet_btag);
ROOT::VecOps::RVec<uint32_t> count_truthjets(int32_t truthid, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth);

//
// Track jet functions

//! Check track jets for overlap cleaning required by b-tagging
/**
 * Overlap is checked between track jets within a large R jet.
 * A track jet i with pT>10 GeV is said to be overlapping if
 * dRij/min(Ri,Rj)<1 for any track jet j with pT>5 GeV within
 * the same largeR jet.
 *
 * A track jet with pT<10 GeV is marked as overlapping. B-tagging
 * is not suported on it anyways.
 *
 * \param fatjet_trkjet_idxs list of track jet indexes associated to large R jets
 * \param trkjet_p4 list of track jet four-momenta
 * \param trkjet_R list of track jet radii
 *
 * \param whether each track jet overlap or not (true = overlaps)
 */
ROOT::VecOps::RVec<bool> trkjet_overlap(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);

//! B-tagging decision on track jet
/**
 * B-tagging passes if largeR jet passes a b-tagging working point
 * and does not overlap with other track jets.
 *
 * \param trkjet_btag list of track jet b-tagging WP decision
 * \param trkjet_overlap list of track jet overlap state
 *
 * \param whether each track jet is b-tagged
 */
ROOT::VecOps::RVec<bool> trkjet_btagged(const std::vector<int>& trkjet_btag, const ROOT::VecOps::RVec<bool>& trkjet_overlap);
#endif // DEFINES_H
