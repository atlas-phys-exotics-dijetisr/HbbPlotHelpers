#ifndef TRUTHDUMPANALYSIS_H
#define TRUTHDUMPANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

#include "EWReweightTool.h"

class TruthDumpAnalysis
{
public:
  /**
   * Construct the selection for the boosted Hbb+jet signal and validation regions
   *
   * \param rootdf base RDF node on which to start the selection
   * \param isMC indicate whether running over MC (true) or data (false)
   * \param syst suffix of a fat jet systematic, leave blank for nominal
   * \param br_truth input branch prefix to save into the output "truth_*" columns
   * \param ewCorrections Name of the file containing the EW corrections for Higgs
   */
  TruthDumpAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false, const std::string& syst="", const std::string& br_truth="Higgs", const std::string& ewCorrections = "");

  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  std::shared_ptr<ROOT::RDF::RNode> df_weight;
private:

  //
  // Tools
  EWReweightTool m_ewCorrections;

  //
  // Settings used to define the selection

  // Simulated events
  bool m_isMC = false;
};

#endif // TRUTHDUMPANALYSIS_H
