#include "SampleMulti.h"

#include <TFile.h>

void SampleMulti::addSample(std::shared_ptr<Sample> sample)
{
  m_samples.push_back(sample);
}

bool SampleMulti::isMC() const
{ return (m_samples.size()>0)?m_samples[0]->isMC():false; }

std::vector<std::string> SampleMulti::filelist()
{
  std::vector<std::string> theFilelist;

  for(std::shared_ptr<Sample> sample : m_samples)
    {
      std::vector<std::string> sFilelist=sample->filelist();
      theFilelist.insert(theFilelist.begin(), sFilelist.begin(), sFilelist.end());
    }
  
  return theFilelist;
}

std::unordered_map<uint32_t, double> SampleMulti::scales()
{
  std::unordered_map<uint32_t, double> theScales;

  for(std::shared_ptr<Sample> sample : m_samples)
    {
      std::unordered_map<uint32_t, double> sScales=sample->scales();
      theScales.insert(sScales.begin(), sScales.end());
    }

  for(std::pair<uint32_t, double> kv : theScales)
    theScales[kv.first]*=m_scale;

  return theScales;
}
std::unordered_map<uint32_t, uint32_t> SampleMulti::weightIdx()
{
  std::unordered_map<uint32_t, uint32_t> theWeightIdx;

  for(std::shared_ptr<Sample> sample : m_samples)
    {
      const std::unordered_map<uint32_t, uint32_t> &sWeightIdx=sample->weightIdx();
      if(m_weightIdxSet)
	{
	  for(const std::pair<uint32_t, uint32_t>& kv : sWeightIdx)
	    theWeightIdx[kv.first]=m_weightIdx;
	}
      else
	theWeightIdx.insert(sWeightIdx.begin(), sWeightIdx.end());
    }
  return theWeightIdx;
}

std::unordered_map<uint32_t, std::vector<uint32_t>> SampleMulti::extraWeights()
{
  std::unordered_map<uint32_t, std::vector<uint32_t>> theExtraWeights;

  for(std::shared_ptr<Sample> sample : m_samples)
    {
      const std::unordered_map<uint32_t, std::vector<uint32_t>> &sExtraWeights=sample->extraWeights();
      if(m_extraWeightsSet)
	{
	  for(const std::pair<uint32_t, std::vector<uint32_t>>& kv : sExtraWeights)
	    theExtraWeights[kv.first]=m_extraWeights;
	}
      else
	theExtraWeights.insert(sExtraWeights.begin(), sExtraWeights.end());
    }
  return theExtraWeights;
}

std::unordered_map<uint32_t, std::vector<double>> SampleMulti::sumW()
{
  std::unordered_map<uint32_t, std::vector<double>> theSumWs;

  for(std::shared_ptr<Sample> sample : m_samples)
    {
      std::unordered_map<uint32_t, std::vector<double>> sSumW=sample->sumW();
      theSumWs.insert(sSumW.begin(), sSumW.end());
    }

  return theSumWs;
}
