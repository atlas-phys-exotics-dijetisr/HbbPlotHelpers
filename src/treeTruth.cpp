#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <getopt.h>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "TruthHbbAnalysis.h"

// -- Options --
uint32_t nthreads=0;
//

void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] outDir [sample [selection]]" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -t, --threads  Number of threads (default: " << nthreads << ")" << std::endl;
  std::cerr << "" << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
        {
          {"threads", required_argument, 0,  't' },
          {0,         0,              0, 0       }
        };

      c = getopt_long(argc, argv, "t:", long_options, &option_index);
      if (c == -1)
        break;

      switch (c)
        {
        case 't':
          nthreads = std::stoi(optarg);
          break;
        default:
          std::cerr << "Invalid option supplied. Aborting." << std::endl;
          std::cerr << std::endl;
          usage(argv);
	  return 1;
        }
    }

  if (optind == argc)
    {
      std::cerr << "Required outDir argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  // Parse arguments
  if(argc<3)
    {
      std::cerr << "usage: " << argv[0] << " outDir [sample [selection]]" << std::endl;
      return 1;
    }
  std::string outDir=argv[optind++];
  std::vector<std::string> saves={};
  std::vector<std::string> selections={"truthsrl","truthsrs"};

  if(optind<argc)
    saves={argv[optind++]};

  if(optind<argc)
    {
      std::stringstream ss(argv[optind++]);
      selections.clear();
      std::string selection;
      while(std::getline(ss,selection,','))
	selections.push_back(selection);
    }
  
  ROOT::EnableImplicitMT(nthreads);  

  // Process stuff
  SampleFactory sf("../truthsamples.yml");

  std::map<std::string, std::shared_ptr<ROOT::RDF::RNode> TruthHbbAnalysis::*> selectionMap;
  selectionMap["truthsrl"]=&TruthHbbAnalysis::df_srl_inc;
  selectionMap["truthsrs"]=&TruthHbbAnalysis::df_srs_inc;

  for(const std::string& save : saves)
    {
      std::cout << "Processing " << save << std::endl;
      if(!sf.contains(save))
	{
	  std::cerr << "WARNING: Unable to find \"" << save << "\"! Skip..." << std::endl;
	  continue;
	}

      std::vector<std::string> branchList={"Hcand_pt","Hcand_m","runNumber","w"};

      std::string outFilePrefix=outDir+"/"+save+"_";
      std::string outFileSuffix=".root";
      std::string outTree="outTree";

      std::shared_ptr<ROOT::RDF::RNode> df=sf[save]->dataframe();
      uint32_t nevents=df->Count().GetValue();
      TruthHbbAnalysis a(df,sf[save]->isMC());

      //
      // Add any branches that depend on sample
      if(sf[save]->isMC())
	{
	  branchList.push_back("mcChannelNumber");
	  if(save.find("Higgs")!=std::string::npos)
	    branchList.push_back("Higgs_pt");
	}	  

      //
      // Loop the selection loop!
      for(const std::string& selection : selections)
	{
	  std::cout << "\t" << selection << std::endl;
	  if(selectionMap.find(selection)==selectionMap.end())
	    {
	      std::cerr << "WARNING: Unable to find \"" << selection << "\"! Skip..." << std::endl;
	      continue;
	    }
  
	  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	  (a.*(selectionMap[selection]))->Snapshot(outTree,outFilePrefix+selection+outFileSuffix,branchList);
	  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	  std::cout << "\t\t" << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms, " << nevents / std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " kHz" << std::endl;
	}
    }
}
