#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <getopt.h>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "Defines.h"
#include "EventWideHbbAnalysis.h"

// -- Options --
uint32_t nthreads=0;
bool btagging_syst = false;
std::string reweightMapName;
std::string ewCorrections;
//

void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] outDir [sample [systematic [selection]]]" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -t, --threads  Number of threads (default: " << nthreads << ")" << std::endl;
  std::cerr << " -b, --btagsys  Run btagging systematics"  << std::endl;
  std::cerr << " -r, --reweight Path to file containing (pT/m) reweight maps (default: none)" << std::endl;
  std::cerr << " -e, --ewcorr Path to file containing EW corrections (default: none)" << std::endl;
  std::cerr << "" << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
        {
          {"threads" ,  required_argument, 0,  't' },
          {"btagsys" ,  no_argument      , 0,  'b' },
          {"reweight",  required_argument, 0,  'r' },
	  {"ewcorr"  ,  required_argument, 0,  'e' },
          {         0,                  0, 0,   0  }
        };

      c = getopt_long(argc, argv, "t:br:e:", long_options, &option_index);
      if (c == -1)
        break;

      switch (c)
        {
        case 't':
          nthreads = std::stoi(optarg);
          break;
        case 'b':
          btagging_syst = true;
          break;
        case 'r':
          reweightMapName=optarg;
          break;
        case 'e':
	  ewCorrections=optarg;
          break;
        default:
          std::cerr << "Invalid option supplied. Aborting." << std::endl;
          std::cerr << std::endl;
          usage(argv);
	  return 1;
        }
    }

  if (optind == argc)
    {
      std::cerr << "Required outDir argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  // Parse arguments
  std::string outDir=argv[optind++];
  std::vector<std::string> saves={"data","Pythia8_dijet","Higgs","Pythia8_ttbar","Sherpa_Zqq","Sherpa_Wqq"};
  std::vector<std::string> systematics={""};
  std::vector<std::string> selections={"srl","srs","vrl","vrs"};

  if(optind<argc)
    saves={argv[optind++]};

  if(optind<argc)
    {
      std::string systematic=argv[optind++];
      if(systematic=="nominal")
	systematics={""};
      else
	systematics={systematic};
    }

  if(optind<argc)
    {
      std::stringstream ss(argv[optind++]);
      selections.clear();
      std::string selection;
      while(std::getline(ss,selection,','))
	selections.push_back(selection);
    }
  
  ROOT::EnableImplicitMT(nthreads);  

  // Process stuff
  SampleFactory sf("../samples.yml");

  std::map<std::string, std::shared_ptr<ROOT::RDF::RNode> EventWideHbbAnalysis::*> selectionMap;
  selectionMap["srl"]=&EventWideHbbAnalysis::df_srl_inc;
  selectionMap["srs"]=&EventWideHbbAnalysis::df_srs_inc;
  selectionMap["vrl"]=&EventWideHbbAnalysis::df_vrl_inc;
  selectionMap["vrs"]=&EventWideHbbAnalysis::df_vrs_inc;

  for(const std::string& save : saves)
    {
      std::cout << "Processing " << save << std::endl;
      if(!sf.contains(save))
	{
	  std::cerr << "WARNING: Unable to find \"" << save << "\"! Skip..." << std::endl;
	  continue;
	}

      // Truth particle of interest from sample name
      std::string br_truth="Higgs";
      std::string br_truthTag="nHBosons";
      if(save.find("Higgs")!=std::string::npos)
	{
	  br_truth="Higgs";
	  br_truthTag="nHBosons";
	}
      if(save.find("Wqq"  )!=std::string::npos)
	{
	  br_truth="Wboson";
	  br_truthTag="nWBosons";
	}
      if(save.find("Zqq"  )!=std::string::npos)
	{
	  br_truth="Zboson";
	  br_truthTag="nZBosons";
	}

      for(const std::string& fatjetSyst : systematics)
	{
	  std::vector<std::string> branchList={"runNumber","eventNumber","w",
					       "Hcand_pt","Hcand_m","Hcand_eta","Hcand_phi","Hcand_uncorr_pt","Hcand_uncorr_m"
	  };

	  std::string outFilePrefix=outDir+"/"+save+"_";
	  std::string outFileSuffix=".root";
	  std::string outTree="outTree";

	  //
	  // Initialize the analysis class

	  // Input based on systematics
	  if(fatjetSyst.empty())
	    {
	      std::cout << "\tnominal" << std::endl;
	    }
	  else
	    {
	      std::cout << "\tsystematic " << fatjetSyst << std::endl;
	      outTree+=fatjetSyst;
	      outFileSuffix="_"+fatjetSyst+outFileSuffix;
	    }

	  // Make it!
	  std::shared_ptr<ROOT::RDF::RNode> df=sf[save]->dataframe();
	  uint32_t nevents=df->Count().GetValue();

	  EventWideHbbAnalysis a(df, sf[save]->isMC(), fatjetSyst, br_truth, br_truthTag, "MV2c10_FixedCutBEff_77", sf[save]->sumW(), reweightMapName, ewCorrections);

	  //
	  // Add any branches that depend on sample/systematic
	  if(sf[save]->isMC())
	    {
	      branchList.push_back("mcChannelNumber");
	      branchList.push_back("mcEventWeight");
	      branchList.push_back("truth_pt" );
	      branchList.push_back("truth_eta");
	      branchList.push_back("truth_phi");
	    }

	  // Add special branches for nominal only
	  if(fatjetSyst.empty() && sf[save]->isMC())
	    {
	      // extra weights
	      for(const std::string& wname : a.extra_weights)
		branchList.push_back("w"+wname);
	      branchList.push_back("wNOEW");

	      // truth counts
	      branchList.push_back("Hcand_nTruthB"     );
	      branchList.push_back("Hcand_nTruthC"     );
	      branchList.push_back("Hcand_nTruthL"     );
	      branchList.push_back("Hcand_nTruthX"     );
	      branchList.push_back("Hcand_nTruthBosons");

	      // Reweighting branch
	      if(!reweightMapName.empty())
		branchList.push_back("reweight");
	    }

	  // Add special branches for nominal data and MC
	  if(fatjetSyst.empty())
	    {
	      // extra variables to further purify Hcand
	      branchList.push_back("Hcand_C2");
	      branchList.push_back("Hcand_trkjet0_isMV2c10_85");
	      branchList.push_back("Hcand_trkjet1_isMV2c10_85");
	    }
	  
	  // btagging systematics
	  if(sf[save]->isMC() && btagging_syst)
	    {
	      for(const std::string& btagsyst : btagging_systematics.at("MV2c10_FixedCutBEff_77"))
		{
		  if(btagsyst.empty()) continue;
		  branchList.push_back("w"+btagsyst);
		}
	    }

	  //
	  // Loop the loop!
	  for(const std::string& selection : selections)
	    {
	      std::cout << "\t\t" << selection << std::endl;
	      if(selectionMap.find(selection)==selectionMap.end())
		{
		  std::cerr << "WARNING: Unable to find \"" << selection << "\"! Skip..." << std::endl;
		  continue;
		}
	      
	      std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	      (a.*(selectionMap[selection]))->Snapshot(outTree,outFilePrefix+selection+outFileSuffix,branchList);
	      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	      std::cout << "\t\t" << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms, " << nevents / std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " kHz" << std::endl;
	    }
	}
    }

  return 0;
}
