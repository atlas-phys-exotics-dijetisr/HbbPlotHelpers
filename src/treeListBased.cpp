#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "ListBasedHbbAnalysis.h"

int main(int argc, char* argv[])
{
  // Parse arguments
  if(argc!=2)
    {
      std::cerr << "usage: " << argv[0] << " nthreads" << std::endl;
      return 1;
    }
  uint32_t nthreads=std::atoi(argv[1]);

  ROOT::EnableImplicitMT(nthreads);  

  // Process stuff
  SampleFactory sf("../samples.yml");
  
  // Samples to save
  std::vector<std::string> saves={"data","Pythia8_dijet","Higgs","Pythia8_ttbar","Sherpa_Zqq","Sherpa_Wqq","Herwig_Wqq","Herwig_Zqq"};

  std::vector<std::string> systs={"",
				  // "JET_CombMass_Baseline__1up",
				  // "JET_CombMass_Baseline__1down",
				  // "JET_CombMass_Modelling__1up",
				  // "JET_CombMass_Modelling__1down",
				  // "JET_CombMass_TotalStat__1up",
				  // "JET_CombMass_TotalStat__1down",
				  // "JET_CombMass_Tracking1__1up",
				  // "JET_CombMass_Tracking1__1down",
				  // "JET_CombMass_Tracking2__1up",
				  // "JET_CombMass_Tracking2__1down",
				  // "JET_CombMass_Tracking3__1up",
				  // "JET_CombMass_Tracking3__1down",
				  // "JET_EffectiveNP_R10_Detector1__1up",
				  // "JET_EffectiveNP_R10_Detector1__1down",
				  // "JET_EffectiveNP_R10_Detector2__1up",
				  // "JET_EffectiveNP_R10_Detector2__1down",
				  // "JET_EffectiveNP_R10_Mixed1__1up",
				  // "JET_EffectiveNP_R10_Mixed1__1down",
				  // "JET_EffectiveNP_R10_Mixed2__1up",
				  // "JET_EffectiveNP_R10_Mixed2__1down",
				  // "JET_EffectiveNP_R10_Mixed3__1up",
				  // "JET_EffectiveNP_R10_Mixed3__1down",
				  // "JET_EffectiveNP_R10_Mixed4__1up",
				  // "JET_EffectiveNP_R10_Mixed4__1down",
				  // "JET_EffectiveNP_R10_Modelling1__1up",
				  // "JET_EffectiveNP_R10_Modelling1__1down",
				  // "JET_EffectiveNP_R10_Modelling2__1up",
				  // "JET_EffectiveNP_R10_Modelling2__1down",
				  // "JET_EffectiveNP_R10_Modelling3__1up",
				  // "JET_EffectiveNP_R10_Modelling3__1down",
				  // "JET_EffectiveNP_R10_Modelling4__1up",
				  // "JET_EffectiveNP_R10_Modelling4__1down",
				  // "JET_EffectiveNP_R10_Statistical1__1up",
				  // "JET_EffectiveNP_R10_Statistical1__1down",
				  // "JET_EffectiveNP_R10_Statistical2__1up",
				  // "JET_EffectiveNP_R10_Statistical2__1down",
				  // "JET_EffectiveNP_R10_Statistical3__1up",
				  // "JET_EffectiveNP_R10_Statistical3__1down",
				  // "JET_EffectiveNP_R10_Statistical4__1up",
				  // "JET_EffectiveNP_R10_Statistical4__1down",
				  // "JET_EffectiveNP_R10_Statistical5__1up",
				  // "JET_EffectiveNP_R10_Statistical5__1down",
				  // "JET_EffectiveNP_R10_Statistical6__1up",
				  // "JET_EffectiveNP_R10_Statistical6__1down",
				  // "JET_EtaIntercalibration_Modelling__1up",
				  // "JET_EtaIntercalibration_Modelling__1down",
				  // "JET_EtaIntercalibration_R10_TotalStat__1up",
				  // "JET_EtaIntercalibration_R10_TotalStat__1down",
				  // "JET_Flavor_Composition__1up",
				  // "JET_Flavor_Composition__1down",
				  // "JET_Flavor_Response__1up",
				  // "JET_Flavor_Response__1down",
				  // "JET_LargeR_TopologyUncertainty_V__1up",
				  // "JET_LargeR_TopologyUncertainty_V__1down",
				  // "JET_LargeR_TopologyUncertainty_top__1up",
				  // "JET_LargeR_TopologyUncertainty_top__1down",
				  // "JET_MassRes_Hbb_comb__1up",
				  // "JET_MassRes_Hbb_comb__1down",
				  // "JET_MassRes_Top_comb__1up",
				  // "JET_MassRes_Top_comb__1down",
				  // "JET_MassRes_WZ_comb__1up",
				  // "JET_MassRes_WZ_comb__1down",
				  // "JET_SingleParticle_HighPt__1up",
				  // "JET_SingleParticle_HighPt__1down"
  };

  for(const std::string& save : saves)
    {
      std::cout << "Processing " << save << std::endl;
      for(const std::string& syst : systs)
	{

	  std::shared_ptr<ROOT::RDF::RNode> df=sf[save]->dataframe();
	  uint32_t nevents=df->Count().GetValue();
	  ListBasedHbbAnalysis a(df,sf[save]->isMC(),syst);

	  ROOT::RDF::RSnapshotOptions opts;
	  opts.fMode = "UPDATE";

	  std::chrono::steady_clock::time_point beginvrl = std::chrono::steady_clock::now();
	  a.df_vrl->Snapshot("vrl/"+syst+"/outTree","/global/cscratch1/sd/kkrizka/results/LB/"+save+".root",{"Hcand_pt","Hcand_m","w"});
	  std::chrono::steady_clock::time_point endvrl = std::chrono::steady_clock::now();
	  std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(endvrl - beginvrl).count() << " ms, " << nevents / std::chrono::duration_cast<std::chrono::milliseconds>(endvrl - beginvrl).count() << " Hz" << std::endl;

	  std::chrono::steady_clock::time_point beginvrs = std::chrono::steady_clock::now();
	  a.df_vrs->Snapshot("vrs/"+syst+"/outTree","/global/cscratch1/sd/kkrizka/results/LB/"+save+".root",{"Hcand_pt","Hcand_m","w"},opts);
	  std::chrono::steady_clock::time_point endvrs = std::chrono::steady_clock::now();
	  std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(endvrs - beginvrs).count() << " ms, " << nevents / std::chrono::duration_cast<std::chrono::milliseconds>(endvrs - beginvrs).count() << " Hz" << std::endl;

	  if(save!="data")
	    {
	      std::chrono::steady_clock::time_point beginsrl = std::chrono::steady_clock::now();
	      a.df_srl->Snapshot("srl/"+syst+"/outTree","/global/cscratch1/sd/kkrizka/results/LB/"+save+".root",{"Hcand_pt","Hcand_m","w"},opts);
	      std::chrono::steady_clock::time_point endsrl = std::chrono::steady_clock::now();
	      std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(endsrl - beginsrl).count() << " ms, " << nevents / std::chrono::duration_cast<std::chrono::milliseconds>(endsrl - beginsrl).count() << " Hz" << std::endl;

	      std::chrono::steady_clock::time_point beginsrs = std::chrono::steady_clock::now();
	      a.df_srs->Snapshot("srs/"+syst+"/outTree","/global/cscratch1/sd/kkrizka/results/LB/"+save+".root",{"Hcand_pt","Hcand_m","w"},opts);
	      std::chrono::steady_clock::time_point endsrs = std::chrono::steady_clock::now();
	      std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(endsrs - beginsrs).count() << " ms, " << nevents / std::chrono::duration_cast<std::chrono::milliseconds>(endsrs - beginsrs).count() << " Hz" << std::endl;
	    }
	}
    }
}
