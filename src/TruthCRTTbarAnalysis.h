#ifndef TRUTHCRTTBARANALYSIS_H
#define TRUTHCRTTBARANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

class TruthCRTTbarAnalysis
{
public:
  TruthCRTTbarAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false);

  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  std::shared_ptr<ROOT::RDF::RNode> df_weight;
  std::shared_ptr<ROOT::RDF::RNode> df_filtered;
  std::shared_ptr<ROOT::RDF::RNode> df_defp4;
  std::shared_ptr<ROOT::RDF::RNode> df_trigger;
  std::shared_ptr<ROOT::RDF::RNode> df_tagjet;
  std::shared_ptr<ROOT::RDF::RNode> df_probejet;
  std::shared_ptr<ROOT::RDF::RNode> df_Hcand;

private:
  bool m_isMC = false;
};

#endif // TRUTHCRTTBARANALYSIS_H
