#include "ListBasedHbbAnalysis.h"

#include "Defines.h"

bool trigger_jet(const std::vector<float>& fatjet_pt, const std::vector<float>& fatjet_m)
{
  for(uint32_t i=0;i<fatjet_pt.size();i++)
    {
      if(fatjet_pt[i]>440 && fatjet_m[i]>45) return true;
    }
  return false;
}

ListBasedHbbAnalysis::ListBasedHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& syst)
{
  df_root=rootdf;

  // Definitions of four-momenta
  df_defp4=std::make_shared<ROOT::RDF::RNode>((*df_root)
					      .Define("fatjet"+syst+"_p4", PtEtaPhiM, {"nfatjet"+syst,"fatjet"+syst+"_pt","fatjet"+syst+"_eta","fatjet"+syst+"_phi","fatjet"+syst+"_m"})
					      .Define("fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_p4", PtEtaPhiE, {"nfatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_pt","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_eta","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_phi","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_E"})
					      .Define("muon_p4", PtEtaPhiM, {"nmuon","muon_pt","muon_eta","muon_phi","muon_m"}));

  df_prw=std::make_shared<ROOT::RDF::RNode>((*df_defp4).Define("weight_prw","weight_norm*weight_pileup"));

  df_trigger=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						.Filter("trigger_HLT_j360_a10_lcw_sub_L1J100 || trigger_HLT_j420_a10_lcw_L1J100 || trigger_HLT_j390_a10t_lcw_jes_30smcINF_L1J100 || trigger_HLT_j420_a10t_lcw_jes_35smcINF_L1J100", "Trigger")
						);

  if(isMC)
    df_lumi=std::make_shared<ROOT::RDF::RNode>((*df_trigger)
					       .Define("weight_trigger", [](int runNumber, double weight)->double {
						   switch(runNumber)
						     {
						     case 284500: return 36.2e3*weight; //mc16a
						     case 300000: return 44.3e3*weight; //mc16d
						     case 310000: return 58.5e3*weight; //mc16e
						     }
						   return 1.;
						 }, {"runNumber", "weight_prw"})
					       );
  else
    df_lumi=std::make_shared<ROOT::RDF::RNode>((*df_triggerjet)
					       .Define("weight_trigger", []()->double { return 1.; })
					       );

  df_triggerjet=std::make_shared<ROOT::RDF::RNode>((*df_lumi)
						   .Filter(trigger_jet, {"fatjet"+syst+"_pt","fatjet"+syst+"_m"} , "Trigger Jet")
						   );  

  df_cleaning=std::make_shared<ROOT::RDF::RNode>((*df_triggerjet)
						 .Filter("eventClean_LooseBad", "Jet Cleaning")
						 );

  df_dijet=std::make_shared<ROOT::RDF::RNode>((*df_cleaning).Filter("nfatjet"+syst+">=2 && fatjet"+syst+"_pt[1]>200", "Dijet"));

  df_trkJet10Idx=std::make_shared<ROOT::RDF::RNode>((*df_dijet).Define("fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", fatjet_trkjet10_idxs, {"fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_pt"}));    

  df_fatjet_idx=std::make_shared<ROOT::RDF::RNode>((*df_trkJet10Idx).Define("fatjet"+syst+"_idx", indexlist, {"nfatjet"+syst}));

  // LargeR jet list
  df_fatjet_ntj  =std::make_shared<ROOT::RDF::RNode>((*df_fatjet_idx).Define("fatjet"+syst+"_ntj"  , count_subjet  , {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet"}));
  df_fatjet_nbin2=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_ntj).Define("fatjet"+syst+"_nbin2", count_bjetsin2, {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_is_MV2c10_FixedCutBEff_77"}));
  
  df_fatjet_vrolap=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_nbin2)
						      .Define("fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_R", trackjet_VR, {"fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_pt"})
						      .Define("fatjet"+syst+"_VR_overlap", fatjet_VR_overlap, {"fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_p4","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_R"})
						      );

  df_fatjet_boost=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_vrolap)
						     .Define("fatjet"+syst+"_boost",fatjet_boost,{"fatjet"+syst+"_pt","fatjet"+syst+"_m"})
						     );


  df_fatjetlist=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_boost)
						   .Define("fatjetlist_idx", "fatjet"+syst+"_idx[(fatjet"+syst+"_ntj>=2)&&(fatjet"+syst+"_VR_overlap==false)&&(fatjet"+syst+"_pt>250)&&(fatjet"+syst+"_m>=45)&&(fatjet"+syst+"_boost<1)]")
						   .Filter("fatjetlist_idx.size()>0", "Fat Jet List")
						   );

  // B-tagging
  df_category =std::make_shared<ROOT::RDF::RNode>((*df_fatjetlist)
						  .Define("SRL","fatjetlist_idx.size()>0 && fatjet"+syst+"_nbin2[fatjetlist_idx[0]]==2")
						  .Define("SRS","!SRL && (fatjetlist_idx.size()>1 && fatjet"+syst+"_nbin2[fatjetlist_idx[1]]==2)")
						  .Define("VRL","(!SRL && !SRS) && ((fatjetlist_idx.size()==1 && fatjet"+syst+"_nbin2[fatjetlist_idx[0]]==0) || (fatjetlist_idx.size()>1 && fatjet"+syst+"_nbin2[fatjetlist_idx[0]]==0 && fatjet"+syst+"_nbin2[fatjetlist_idx[1]]==0))")
						  .Define("VRS","(!SRL && !SRS) && (fatjetlist_idx.size()>1 && fatjet"+syst+"_nbin2[fatjetlist_idx[0]]==0 && fatjet"+syst+"_nbin2[fatjetlist_idx[1]]==0)")
						  .Filter("SRL || SRS || VRL || VRS", "Region")
						  );

  if(isMC)
    {
      df_sf_srl = std::make_shared<ROOT::RDF::RNode>((*df_category)
						     .Filter("SRL","SRL")
						     .Define("w", [](double weight, const ROOT::VecOps::RVec<uint32_t>& fatjetlist_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->double {
							 return weight*trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[0]][0]][0]*trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[0]][1]][0];
						       },{"weight_trigger","fatjetlist_idx","fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_SF_MV2c10_FixedCutBEff_77"})
						     );

      df_sf_srs = std::make_shared<ROOT::RDF::RNode>((*df_category)
						     .Filter("SRS","SRS")
						     .Define("w", [](double weight, const ROOT::VecOps::RVec<uint32_t>& fatjetlist_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->double {
							 return weight
							   *trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[0]][0]][0]*trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[0]][1]][0]
							   *trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[1]][0]][0]*trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[1]][1]][0];
						       },{"weight_trigger","fatjetlist_idx","fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_SF_MV2c10_FixedCutBEff_77"})
						     );

      df_sf_vrl = std::make_shared<ROOT::RDF::RNode>((*df_category)
						     .Filter("VRL","VRL")
						     .Define("w", [](double weight, const ROOT::VecOps::RVec<uint32_t>& fatjetlist_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->double {
							 return weight
							   *trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[0]][0]][0]*trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[0]][1]][0]
							   *((fatjetlist_idx.size()>1)?trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[1]][0]][0]*trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[1]][1]][0]:1.);
						       },{"weight_trigger","fatjetlist_idx","fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_SF_MV2c10_FixedCutBEff_77"})
						     );

      df_sf_vrs = std::make_shared<ROOT::RDF::RNode>((*df_category)
						     .Filter("VRS","VRS")
						     .Define("w", [](double weight, const ROOT::VecOps::RVec<uint32_t>& fatjetlist_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->double {
							 return weight
							   *trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[0]][0]][0]*trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[0]][1]][0]
							   *trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[1]][0]][0]*trkjet_sf[fatjet_trkjet_idxs[fatjetlist_idx[1]][1]][0];
						       },{"weight_trigger","fatjetlist_idx","fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","fatjet"+syst+"_GhostVR30Rmax4Rmin02TrackJet_SF_MV2c10_FixedCutBEff_77"})
						     );
    }
  else
    {
      df_sf_srl = std::make_shared<ROOT::RDF::RNode>((*df_category)
						     .Filter("SRL","SRL")
						     .Define("w", []()->double { return 1.; })
						     );

      df_sf_srs = std::make_shared<ROOT::RDF::RNode>((*df_category)
						     .Filter("SRS","SRS")
						     .Define("w", []()->double { return 1.; })
						     );

      df_sf_vrl = std::make_shared<ROOT::RDF::RNode>((*df_category)
						     .Filter("VRL","VRL")
						     .Define("w", []()->double { return 1.; })
						     );

      df_sf_vrs = std::make_shared<ROOT::RDF::RNode>((*df_category)
						     .Filter("VRS","VRS")
						     .Define("w", []()->double { return 1.; })
						     );
    }

  // Region categorization
  df_srl=std::make_shared<ROOT::RDF::RNode>((*df_sf_srl)
					    .Define("Hcand_idx","fatjetlist_idx[0]")
					    .Define("Hcand_pt","fatjet"+syst+"_pt[Hcand_idx]")
					    .Define("Hcand_m","fatjet"+syst+"_m[Hcand_idx]")
					    //.Filter("Hcand_pt>440","Hcand pT")
					    //.Filter("Hcand_m>60", "Hcand M")
					    );

  df_srs=std::make_shared<ROOT::RDF::RNode>((*df_sf_srs)
					    .Define("Hcand_idx","fatjetlist_idx[1]")
					    .Define("Hcand_pt","fatjet"+syst+"_pt[Hcand_idx]")
					    .Define("Hcand_m","fatjet"+syst+"_m[Hcand_idx]")
					    //.Filter("Hcand_pt>440","Hcand pT")
					    //.Filter("Hcand_m>60", "Hcand M")
					    );
  
  df_vrl=std::make_shared<ROOT::RDF::RNode>((*df_sf_vrl)
					    .Define("Hcand_idx","fatjetlist_idx[0]")
					    .Define("Hcand_pt","fatjet"+syst+"_pt[Hcand_idx]")
					    .Define("Hcand_m","fatjet"+syst+"_m[Hcand_idx]")
					    //.Filter("Hcand_pt>440","Hcand pT")
					    //.Filter("Hcand_m>60", "Hcand M")
					    );

  df_vrs=std::make_shared<ROOT::RDF::RNode>((*df_sf_vrs)
					    .Define("Hcand_idx","fatjetlist_idx[1]")
					    .Define("Hcand_pt","fatjet"+syst+"_pt[Hcand_idx]")
					    .Define("Hcand_m","fatjet"+syst+"_m[Hcand_idx]")
					    //.Filter("Hcand_pt>440","Hcand pT")
					    //.Filter("Hcand_m>60", "Hcand M")
					    );
}
