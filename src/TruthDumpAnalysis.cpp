#include "TruthDumpAnalysis.h"

#include <functional>

#include "Defines.h"

TruthDumpAnalysis::TruthDumpAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& syst, const std::string& br_truth, const std::string& ewCorrections)
  : m_isMC(isMC), m_ewCorrections(ewCorrections)
{
  df_root=rootdf;

  // Definitions of weights
  if(isMC)
    {
      df_weight=std::make_shared<ROOT::RDF::RNode>((*df_root)
						   .Define("truth_pt", [](float truth_pt)->float { return truth_pt; }, {br_truth+"_pt"})
						   .Define("ewcorr", [this](int mcChannelNumber, float truth_pt, const std::vector<float>& mcEventWeights)->float
								     { return this->m_ewCorrections.calcEWCorrection(mcChannelNumber,truth_pt,mcEventWeights); },
						     {"mcChannelNumber", "truth_pt", "mcEventWeights"})
						   .Define("w", [](float weight, float ewcorr)->float
							   { return weight*ewcorr; },
							   {"weight_norm","ewcorr"})
						   .Define("wNOEW", [](float weight)->float
							   { return weight; },
							   {"weight_norm"})
						   );
    }
  else
    {
      df_weight=std::make_shared<ROOT::RDF::RNode>((*df_root)
						   .Define("w", []()->float { return 1.; })
						   );
    }
}
