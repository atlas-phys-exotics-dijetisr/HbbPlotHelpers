#include "TruthCRTTbarAnalysis.h"

#include "Defines.h"

TruthCRTTbarAnalysis::TruthCRTTbarAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC)
  : m_isMC(isMC)
{
  df_root=rootdf;

  // Definitions of weights
  if(isMC)
    {
      df_weight=std::make_shared<ROOT::RDF::RNode>((*df_root)
                                                   .Define("w", [](float weight)->double
                                                           { return weight*(36.2e3+41.0e3+58.5e3); },
                                                           {"weight_norm"})
                                                   );
    }
  else
    {
      df_weight=std::make_shared<ROOT::RDF::RNode>((*df_root)
                                                   .Define("w", []()->double { return 1.; })
                                                   );
    }

  // Filter out events without at least one muon and two fatjets w/ pt > 250.0
  df_filtered = std::make_shared<ROOT::RDF::RNode> ((*df_weight)
                                                    .Filter([](int32_t   nmuon)->bool {return nmuon>0;},   {"nmuon"})
                                                    .Filter([](int32_t nfatjet)->bool {return nfatjet>1;}, {"nfatjet"})
                                                    .Filter([](int32_t    njet)->bool {return njet>0;},    {"njet"})
                                                    .Filter([](const std::vector<float>& fatjet_pt)->bool
                                                            { return fatjet_pt[0] > 250.0 && fatjet_pt[1] > 250.0; },
                                                            {"fatjet_pt"})
                                                    );

  df_defp4   = std::make_shared<ROOT::RDF::RNode> ((*df_filtered)
                                                   .Define("jet_p4"   , PtEtaPhiE,
                                                           {"njet","jet_pt","jet_eta","jet_phi","jet_E"})
                                                   .Define("fatjet_p4", PtEtaPhiM, 
                                                           {"nfatjet","fatjet_pt","fatjet_eta","fatjet_phi","fatjet_m"}) 
                                                   .Define("muon_p4"  , PtEtaPhiE,
                                                           {"nmuon", "muon_pt", "muon_eta", "muon_phi", "muon_E"})
                                                   .Define("fatjet_nBHadrons", CountHadrons, {"jet_p4", "fatjet_p4", "jet_GhostBHadronsFinalCount"}) 
                                                   );

  // Offline muon trigger cut
  df_trigger  = std::make_shared<ROOT::RDF::RNode>((*df_defp4)
                                                   .Filter([](const std::vector<float>& muon_pt)->bool { return muon_pt[0]>52.5;}, {"muon_pt"}) 
                                                   );

  df_tagjet   = std::make_shared<ROOT::RDF::RNode> ((*df_trigger)
                                                    .Define("fatjet_muon0_dR", [](const ROOT::VecOps::RVec<TLorentzVector>& muon_p4, 
                                                                               const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4
                                                                               )->ROOT::VecOps::RVec<float> 
                                                            {
                                                             ROOT::VecOps::RVec<float> dR(fatjet_p4.size());
                                                             std::transform(fatjet_p4.begin(), fatjet_p4.end(), dR.begin(), 
                                                                            [&muon_p4](const TLorentzVector& fatjet_p4) { return muon_p4[0].DeltaR(fatjet_p4); });
                                                             return dR;
                                                            }, {"muon_p4", "fatjet_p4"})
                                                    .Define("tagjet_idx", [](const ROOT::VecOps::RVec<float>& fatjet_muon0_dR)->uint32_t 
                                                            {
                                                             return std::distance(fatjet_muon0_dR.begin(),
                                                                                  std::min_element(fatjet_muon0_dR.begin(),fatjet_muon0_dR.end()));
                                                            }, {"fatjet_muon0_dR"})
                                                    .Filter([](const ROOT::VecOps::RVec<float> fatjet_muon0_dR,
                                                               const uint32_t tagjet_idx
                                                               )->bool
                                                            {
                                                             return fatjet_muon0_dR[tagjet_idx]<1.5;
                                                            }, {"fatjet_muon0_dR", "tagjet_idx"})
                                                    .Filter([](const std::vector<int>& fatjet_nBHadrons, const uint32_t tagjet_idx)->bool {return fatjet_nBHadrons[tagjet_idx]>0;}, {"fatjet_nBHadrons","tagjet_idx"})
                                                   );

  df_probejet = std::make_shared<ROOT::RDF::RNode> ((*df_tagjet)
                                                    .Define("fatjet_tagjet_dR", [](const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4, 
                                                                                   const uint32_t tagjet_idx
                                                                                   )->ROOT::VecOps::RVec<float>
                                                            {
                                                             ROOT::VecOps::RVec<float> dR;
                                                             for(int i=0;i<fatjet_p4.size();++i){
                                                               if(i==tagjet_idx) continue;
                                                               dR.push_back(fatjet_p4[tagjet_idx].DeltaR(fatjet_p4[i]));
                                                             } 
                                                             return dR;
                                                            }, {"fatjet_p4", "tagjet_idx"})
                                                    .Define("Hcand_idx", [](const ROOT::VecOps::RVec<float>& fatjet_tagjet_dR)->uint32_t
                                                            {
                                                             return std::distance(fatjet_tagjet_dR.begin(),
                                                                                  std::max_element(fatjet_tagjet_dR.begin(),fatjet_tagjet_dR.end()));
                                                            }, {"fatjet_tagjet_dR"})
                                                    .Filter([](const ROOT::VecOps::RVec<float> fatjet_tagjet_dR,
                                                               const uint32_t Hcand_idx
                                                               )->bool
                                                            {
                                                             return fatjet_tagjet_dR[Hcand_idx]>1.5;
                                                            }, {"fatjet_tagjet_dR", "Hcand_idx"})
                                                    .Filter([](const std::vector<int>& fatjet_nBHadrons, const uint32_t Hcand_idx)->bool {return fatjet_nBHadrons[Hcand_idx]>0;}, {"fatjet_nBHadrons","Hcand_idx"})
                                                    );

  df_Hcand    = std::make_shared<ROOT::RDF::RNode> ((*df_probejet)
                                                    .Filter([](uint32_t Hcand_idx)->bool { return Hcand_idx<2;}, {"Hcand_idx"})
                                                    .Define("Hcand_pt", define_float_idx, {"fatjet_pt", "Hcand_idx"})
                                                    .Define("Hcand_m" , define_float_idx, {"fatjet_m" , "Hcand_idx"})
                                                    );
  
}
