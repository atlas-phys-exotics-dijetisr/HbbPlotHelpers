#include "SampleFactory.h"

#include "SampleFilelist.h"
#include "SampleMulti.h"

#include <iostream>
#include <regex>

SampleFactory::SampleFactory(const std::string& samplesFile)
{
  YAML::Node config = YAML::LoadFile(samplesFile);

  for(const YAML::detail::iterator_value& sampleDef : config)
    {
      std::string sampleName=sampleDef.first.Scalar();
      YAML::Node sampleNode=sampleDef.second;

      // Common properties
      bool isMC=sampleNode["mc"].as<bool>(false);
      
      //
      // Filelist definition
      if(sampleNode["filelists"].IsDefined())
	{
	  if(sampleNode["campaigns"].IsDefined())
	    { // Multiple campaign samples
	      std::shared_ptr<SampleMulti> sample=std::make_shared<SampleMulti>();
	      // Note: Do not decorate the sample as all properties are taken from the filelist subsamples
	      // This would double-apply some values (ie: scale)

	      for(const YAML::Node& campaignNode : sampleNode["campaigns"])
		{
		  std::shared_ptr<SampleFilelist> campsample=std::make_shared<SampleFilelist>(isMC);
		  decorateSample(campsample, sampleNode);

		  // Loop over a dataset
		  for(const YAML::Node& filelistNode : sampleNode["filelists"])
		    {
		      std::string filelist=std::regex_replace(filelistNode.Scalar(), std::regex("CAMP"), campaignNode.Scalar());
		      campsample->addFilelist(filelist);
		    }
		  sample->addSample(campsample);

		  m_samples[sampleName+"."+campaignNode.Scalar()]=campsample;
		}
	      m_samples[sampleName]=sample;
	    }
	  else
	    { // Single sample
	      std::shared_ptr<SampleFilelist> sample=std::make_shared<SampleFilelist>(isMC);
	      decorateSample(sample, sampleNode);

	      // Loop over a dataset
	      for(const YAML::Node& filelistNode : sampleNode["filelists"])
		sample->addFilelist(filelistNode.Scalar());

	      m_samples[sampleName]=sample;
	    }
	}

      //
      // Files definition
      else if(sampleNode["files"].IsDefined())
	{
	  std::shared_ptr<SampleFilelist> sample=std::make_shared<SampleFilelist>(isMC);
	  decorateSample(sample, sampleNode);

	  // Loop over a dataset
	  for(const YAML::Node& fileNode : sampleNode["files"])
	    {
	      sample->addFile(fileNode.Scalar());
	    }

	  m_samples[sampleName]=sample;
	}
      
      //
      // Sample definition
      else if(sampleNode["samples"].IsDefined())
	{
	  std::shared_ptr<SampleMulti> sample=std::make_shared<SampleMulti>();

	  for(const YAML::Node& sampleNode : sampleNode["samples"])
	    {
	      if(m_samples.find(sampleNode.Scalar())==m_samples.end())
		{
		  std::cerr << "ERROR: Cannot find subsample '" << sampleNode.Scalar() << "' for " << sampleName << std::endl;
		  throw std::string("Cannot find subsample '" + sampleNode.Scalar() + "' for " + sampleName);
		}

	      sample->addSample(m_samples[sampleNode.Scalar()]);
	    }

	  decorateSample(sample, sampleNode);	  
	  m_samples[sampleName]=sample;
	}
    }
}

void SampleFactory::decorateSample(std::shared_ptr<Sample> sample, const YAML::Node& sampleNode) const
{
  // Get properties
  bool weighted     =sampleNode["weighted" ].as<bool    >(true);
  double scale      =sampleNode["scale"    ].as<float   >(1.  );
  uint32_t weightIdx=sampleNode["weightIdx"].as<uint32_t>(0   );
  std::vector<uint32_t> extraWeights=sampleNode["extraWeights"].as<std::vector<uint32_t>>(std::vector<uint32_t>());

  std::shared_ptr<SampleFilelist> s_filelist=std::dynamic_pointer_cast<SampleFilelist>(sample);
  if(s_filelist!=nullptr)
    {
      s_filelist->setWeighted (weighted );
    }

  // Set properties  
  sample->setScale    (scale    );
  sample->setWeightIdx(weightIdx);
  if(extraWeights.size()>0)
    sample->setExtraWeights(extraWeights);
}

bool SampleFactory::contains(const std::string& sampleName) const
{
  return m_samples.find(sampleName)!=m_samples.end();
}

std::shared_ptr<Sample> SampleFactory::operator[] (const std::string& sampleName)
{
  return m_samples[sampleName];
}
