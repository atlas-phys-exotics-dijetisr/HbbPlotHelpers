#ifndef SAMPLEFACTORY_H
#define SAMPLEFACTORY_H

#include <string>
#include <unordered_map>
#include <memory>

#include <yaml-cpp/yaml.h>

#include "Sample.h"

class SampleFactory
{
public:
  SampleFactory(const std::string& samplesFile);

  bool contains(const std::string& sampleName) const;

  std::shared_ptr<Sample> operator[] (const std::string& sampleName);

private:
  std::unordered_map<std::string,std::shared_ptr<Sample>> m_samples;


  /**
   * Decorate sample with some information from the YAML node.
   *
   * \param sample Sample object
   * \param sampleNode YAML node with information for the sample
   */
  void decorateSample(std::shared_ptr<Sample> sample, const YAML::Node& sampleNode) const;
};

#endif // SAMPLEFACTORY_H
