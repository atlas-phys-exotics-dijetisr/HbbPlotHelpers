#include "EWReweightTool.h"

EWReweightTool::EWReweightTool(const std::string &ewCorrections)
{
  // Load EW corrections
  if(!ewCorrections.empty())
    {
      m_fEW=TFile::Open(ewCorrections.c_str());

      m_EW_WmH =m_fEW->Get<TF1>("WmH" );
      m_EW_WpH =m_fEW->Get<TF1>("WpH" );
      m_EW_ZllH=m_fEW->Get<TF1>("ZHll");
      m_EW_ZvvH=m_fEW->Get<TF1>("ZHvv");
      m_EW_VH  =m_fEW->Get<TF1>("VH"  );
      m_EW_VBF =m_fEW->Get<TF1>("VBF" );
      m_EW_ttH =m_fEW->Get<TF1>("ttH" );
    }
}

EWReweightTool::~EWReweightTool()
{
  if(m_fEW)
    {
      m_fEW->Close();
      m_fEW=nullptr;
    }
}

float EWReweightTool::calcEWCorrection(int mcChannelNumber, float truth_pt, const std::vector<float>& mcEventWeights)
{
  switch(mcChannelNumber)
    {
      //
      // Higgs corrections from a file
    case 345054:
      return (this->m_EW_WpH )?(1+this->m_EW_WpH ->Eval(truth_pt)):1.;
    case 345053:
      return (this->m_EW_WmH )?(1+this->m_EW_WmH ->Eval(truth_pt)):1.;
    case 346641:
    case 346640:
    case 346639:
      return (this->m_EW_VH  )?(1+this->m_EW_VH  ->Eval(truth_pt)):1.;
    case 345055:
      return (this->m_EW_ZllH)?(1+this->m_EW_ZllH->Eval(truth_pt)):1.;
    case 345056:
      return (this->m_EW_ZvvH)?(1+this->m_EW_ZvvH->Eval(truth_pt)):1.;
    case 345949:
      return (this->m_EW_VBF )?(1+this->m_EW_VBF ->Eval(truth_pt)):1.;
    case 346343:
    case 346344:
    case 346345:
      return (this->m_EW_ttH)?(1+this->m_EW_ttH->Eval(truth_pt)):1.;

      //
      // V+jets corrections from an alternate weight
    case 700040:
    case 700041:
    case 700042:

    case 700043:
    case 700044:
    case 700045:
      return mcEventWeights[298]/mcEventWeights[0];      

      // Others
    default:
      return 1.;
    }
}

