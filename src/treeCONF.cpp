#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "CONFHbbAnalysis.h"

int main(int argc, char* argv[])
{
  // Parse arguments
  if(argc!=2)
    {
      std::cerr << "usage: " << argv[0] << " nthreads" << std::endl;
      return 1;
    }
  uint32_t nthreads=std::atoi(argv[1]);

  ROOT::EnableImplicitMT(nthreads);  

  // Process stuff
  SampleFactory sf("../samples.yml");

  // Samples to save
  std::vector<std::string> saves={"Higgs_ggf"};

  std::vector<std::string> systs={"",
				  // "JET_CombMass_Baseline__1up",
				  // "JET_CombMass_Baseline__1down",
				  // "JET_CombMass_Modelling__1up",
				  // "JET_CombMass_Modelling__1down",
				  // "JET_CombMass_TotalStat__1up",
				  // "JET_CombMass_TotalStat__1down",
				  // "JET_CombMass_Tracking1__1up",
				  // "JET_CombMass_Tracking1__1down",
				  // "JET_CombMass_Tracking2__1up",
				  // "JET_CombMass_Tracking2__1down",
				  // "JET_CombMass_Tracking3__1up",
				  // "JET_CombMass_Tracking3__1down",
				  // "JET_EffectiveNP_R10_Detector1__1up",
				  // "JET_EffectiveNP_R10_Detector1__1down",
				  // "JET_EffectiveNP_R10_Detector2__1up",
				  // "JET_EffectiveNP_R10_Detector2__1down",
				  // "JET_EffectiveNP_R10_Mixed1__1up",
				  // "JET_EffectiveNP_R10_Mixed1__1down",
				  // "JET_EffectiveNP_R10_Mixed2__1up",
				  // "JET_EffectiveNP_R10_Mixed2__1down",
				  // "JET_EffectiveNP_R10_Mixed3__1up",
				  // "JET_EffectiveNP_R10_Mixed3__1down",
				  // "JET_EffectiveNP_R10_Mixed4__1up",
				  // "JET_EffectiveNP_R10_Mixed4__1down",
				  // "JET_EffectiveNP_R10_Modelling1__1up",
				  // "JET_EffectiveNP_R10_Modelling1__1down",
				  // "JET_EffectiveNP_R10_Modelling2__1up",
				  // "JET_EffectiveNP_R10_Modelling2__1down",
				  // "JET_EffectiveNP_R10_Modelling3__1up",
				  // "JET_EffectiveNP_R10_Modelling3__1down",
				  // "JET_EffectiveNP_R10_Modelling4__1up",
				  // "JET_EffectiveNP_R10_Modelling4__1down",
				  // "JET_EffectiveNP_R10_Statistical1__1up",
				  // "JET_EffectiveNP_R10_Statistical1__1down",
				  // "JET_EffectiveNP_R10_Statistical2__1up",
				  // "JET_EffectiveNP_R10_Statistical2__1down",
				  // "JET_EffectiveNP_R10_Statistical3__1up",
				  // "JET_EffectiveNP_R10_Statistical3__1down",
				  // "JET_EffectiveNP_R10_Statistical4__1up",
				  // "JET_EffectiveNP_R10_Statistical4__1down",
				  // "JET_EffectiveNP_R10_Statistical5__1up",
				  // "JET_EffectiveNP_R10_Statistical5__1down",
				  // "JET_EffectiveNP_R10_Statistical6__1up",
				  // "JET_EffectiveNP_R10_Statistical6__1down",
				  // "JET_EtaIntercalibration_Modelling__1up",
				  // "JET_EtaIntercalibration_Modelling__1down",
				  // "JET_EtaIntercalibration_R10_TotalStat__1up",
				  // "JET_EtaIntercalibration_R10_TotalStat__1down",
				  // "JET_Flavor_Composition__1up",
				  // "JET_Flavor_Composition__1down",
				  // "JET_Flavor_Response__1up",
				  // "JET_Flavor_Response__1down",
				  // "JET_LargeR_TopologyUncertainty_V__1up",
				  // "JET_LargeR_TopologyUncertainty_V__1down",
				  // "JET_LargeR_TopologyUncertainty_top__1up",
				  // "JET_LargeR_TopologyUncertainty_top__1down",
				  // "JET_MassRes_Hbb_comb__1up",
				  // "JET_MassRes_Hbb_comb__1down",
				  // "JET_MassRes_Top_comb__1up",
				  // "JET_MassRes_Top_comb__1down",
				  // "JET_MassRes_WZ_comb__1up",
				  // "JET_MassRes_WZ_comb__1down",
				  // "JET_SingleParticle_HighPt__1up",
				  // "JET_SingleParticle_HighPt__1down"
  };

  for(const std::string& save : saves)
    {
      for(const std::string& syst : systs)
	{

	  CONFHbbAnalysis a(sf[save]->dataframe(),true,syst);

	  ROOT::RDF::RSnapshotOptions opts;
	  opts.fMode = "UPDATE";

	  a.df_sr ->Snapshot("sr/" +syst+"/outTree",save+".root",{"Hcand_pt","Hcand_m","w"});
	  a.df_vr ->Snapshot("vr/" +syst+"/outTree",save+".root",{"Hcand_pt","Hcand_m","w"},opts);
	}
    }
}
