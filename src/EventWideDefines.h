#ifndef EVENTWIDEDEFINES_H
#define EVENTWIDEDEFINES_H

#include <stdint.h>

namespace EventWide
{
  bool trigger_data(int32_t runNumber,
		    int8_t HLT_j360_a10_lcw_sub_L1J100, // 2015
		    int8_t HLT_j420_a10_lcw_L1J100, //2016
		    int8_t HLT_j390_a10t_lcw_jes_30smcINF_L1J100, int8_t HLT_j440_a10t_lcw_jes_L1J100, //2017
		    int8_t HLT_j420_a10t_lcw_jes_35smcINF_L1J100, int8_t HLT_j460_a10t_lcw_jes_L1J100 //2018
		    );

  bool trigger_mc(int32_t runNumber,
		  int8_t HLT_j360_a10_lcw_sub_L1J100, // 2015
		  int8_t HLT_j420_a10_lcw_L1J100, //2016
		  int8_t HLT_j390_a10t_lcw_jes_30smcINF_L1J100, int8_t HLT_j440_a10t_lcw_jes_L1J100, //2017
		  int8_t HLT_j420_a10t_lcw_jes_35smcINF_L1J100, int8_t HLT_j460_a10t_lcw_jes_L1J100 //2018
		  );
};

#endif // EVENTWIDEDEFINES_H
