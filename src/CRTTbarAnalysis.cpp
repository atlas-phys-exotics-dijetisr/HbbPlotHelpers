#include "CRTTbarAnalysis.h"

#include "Sample.h"
#include "Defines.h"
#include "CRTTbarDefines.h"

CRTTbarAnalysis::CRTTbarAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& syst,
				 const std::string& br_truth_pt, const std::string& btagWP, const std::unordered_map<uint32_t, std::vector<double>>& SumW,
				 const std::string& reweightMapName, const std::string& ewCorrections)
  : m_isMC(isMC), m_SumW(SumW), m_reweightMapName(reweightMapName), m_ewCorections(ewCorrections)
{
  //
  // Load information

  // V+jets
  m_weightdb.scanFile(700040, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700041, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700042, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700043, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700044, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700045, "../data/weightdb/Sh228.txt");

  // ttbar
  m_weightdb.scanFile(410470, "../data/weightdb/PowPy8_ttbar.txt");
  m_weightdb.scanFile(410471, "../data/weightdb/PowPy8_ttbar.txt");

  //
  // Load reweighting maps
  LoadReweightMaps();

  //
  // Create the RDF graph

  df_root=rootdf;
  
  // Filter out events without at least one muon and two fatjets w/ pt > 250.0
  // TODO: consider adding min muonPt > 40.0 (make it faster)
  df_filtered = std::make_shared<ROOT::RDF::RNode> ((*df_root)
						    .Filter([](int32_t nmuon) { return nmuon>0; }, {"nmuon"})
						    .Filter([](int32_t nfatjet) { return nfatjet>1; }, {"nfatjet"+syst})
						    .Filter([](const std::vector<float>& fatjet_pt)-> bool
							    { return fatjet_pt[0] > 250.0 && fatjet_pt[1] > 250.0; },
							    {"fatjet" + syst + "_pt"}));

  // Definitions of four-momenta
  df_defp4 = std::make_shared<ROOT::RDF::RNode> ((*df_filtered)
						 .Define("fatjet" + syst + "_p4", PtEtaPhiM, 
							 {"nfatjet" + syst, "fatjet" + syst + "_pt", "fatjet" + syst + "_eta","fatjet" + syst + "_phi", "fatjet" + syst + "_m"})
						 .Define("trkjet_p4", PtEtaPhiE,
							 {"ntrkjet", "trkjet_pt", "trkjet_eta", "trkjet_phi", "trkjet_E"})
						 .Define("muon_p4", PtEtaPhiM,
							 {"nmuon", "muon_pt", "muon_eta", "muon_phi", "muon_m"})
						 .Define("muon0_pt" , define_float_idx0, {"muon_pt" })
						 .Define("muon0_eta", define_float_idx0, {"muon_eta"})
						 .Define("muon0_phi", define_float_idx0, {"muon_phi"})
						 );

  df_prw = std::make_shared<ROOT::RDF::RNode> ((*df_defp4)
					       .Define("weight_prw",[](float weight, float weight_pileup)->float
						       { return weight*weight_pileup;}, {"weight_norm", "weight_pileup"}));

  // Calculate EW corrections using an external file
  if(isMC)
    {
      df_prw=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						.Define("truth_pt", [](float truth_pt)->float { return truth_pt; }, {br_truth_pt})
						.Define("ewcorr", [this](int mcChannelNumber, float truth_pt, const std::vector<float>& mcEventWeights)->float
								  {
								    switch(mcChannelNumber)
								      {
									//
									// Higgs corrections from a file
								      case 345054:
									return (this->m_EW_WpH )?(1+this->m_EW_WpH ->Eval(truth_pt)):1.;
								      case 345053:
									return (this->m_EW_WmH )?(1+this->m_EW_WmH ->Eval(truth_pt)):1.;
								      case 346641:
								      case 346640:
								      case 346639:
									return (this->m_EW_VH  )?(1+this->m_EW_VH  ->Eval(truth_pt)):1.;
								      case 345055:
									return (this->m_EW_ZllH)?(1+this->m_EW_ZllH->Eval(truth_pt)):1.;
								      case 345056:
									return (this->m_EW_ZvvH)?(1+this->m_EW_ZvvH->Eval(truth_pt)):1.;
								      case 345949:
									return (this->m_EW_VBF )?(1+this->m_EW_VBF ->Eval(truth_pt)):1.;
								      case 346343:
								      case 346344:
								      case 346345:
									return (this->m_EW_ttH)?(1+this->m_EW_ttH->Eval(truth_pt)):1.;

									//
									// V+jets corrections from an alternate weight
								      case 700040:
								      case 700041:
								      case 700042:

								      case 700043:
								      case 700044:
								      case 700045:
									return mcEventWeights[298]/mcEventWeights[0];

									// Others
								      default:
									return 1.;
								      }
								  }, {"mcChannelNumber", "truth_pt", "mcEventWeights"})
						.Define("weight_ew", [](float weight, float correction)->float { return weight*correction; }, {"weight_prw", "ewcorr"})
						);
    }

  if(isMC)
    df_trigger = std::make_shared<ROOT::RDF::RNode> ((*df_prw)
						     .Filter(filter_trigger_or1, {"trigger_HLT_mu50"})
						     .Define("weight_trigger", [](int runNumber, float weight) -> float {
							 switch(runNumber)
							   { // Need to check these luminosities
							   case 284500: return 36.2e3*weight; //mc16a
							   case 300000: return 44.3e3*weight; //mc16d
							   case 310000: return 58.5e3*weight; //mc16e
							   }
							 return 1.;
						       }, {"runNumber", "weight_ew"}));
  else
    df_trigger = std::make_shared<ROOT::RDF::RNode> ((*df_prw)
						     .Filter(filter_trigger_or1, {"trigger_HLT_mu50"})
						     .Define("weight_trigger", []() -> float { return 1.;}));

  df_triggermuon = std::make_shared<ROOT::RDF::RNode> ((*df_trigger)
						       .Filter([](const std::vector<std::string>& muon_listTrigChains, const std::vector<std::vector<int32_t> >& muon_isTrigMatchedToChain)->bool {
							   for(uint32_t i=0;i<muon_listTrigChains.size();i++)
							     {
							       if(muon_listTrigChains[i]=="HLT_mu50") return muon_isTrigMatchedToChain[0][i];
							     }
							   return false;
							 }, {"muon_listTrigChains","muon_isTrigMatchedToChain"})
						       );
  
  // cleaning
  df_cleaning = std::make_shared<ROOT::RDF::RNode> ((*df_triggermuon)
						    .Filter(filter_flag, {"eventClean_LooseBad"}));
  
  // trackjet overlap
  df_trkjet_btag = std::make_shared<ROOT::RDF::RNode> ((*df_cleaning)
						       .Define("trkjet_R", trackjet_VR, {"trkjet_pt"})
						       .Define("trkjet_overlap", trkjet_overlap,
							       {"fatjet" + syst + "_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_p4", "trkjet_R"})
						       .Define("trkjet_btag", trkjet_btagged,
							       {"trkjet_is_"+btagWP, "trkjet_overlap" })
						       );

  // tag muons
  df_muon_pt = std::make_shared<ROOT::RDF::RNode> ((*df_trkjet_btag)
						   .Filter([](float muon_pt)->bool { return muon_pt>1.05*50; }, {"muon0_pt"}, "muon0_pt")
						   .Define("trkjet_muon0_dR", [](const ROOT::VecOps::RVec<TLorentzVector>& muon_p4, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4)->ROOT::VecOps::RVec<float> {
						       ROOT::VecOps::RVec<float> dRVR(trkjet_p4.size());
						       std::transform(trkjet_p4.begin(), trkjet_p4.end(), dRVR.begin(), [&muon_p4](const TLorentzVector& trkjet_p4) { return muon_p4[0].DeltaR(trkjet_p4); });
						       return dRVR;
						     }, {"muon_p4", "trkjet_p4"})
						   .Define("muon0_trkjet_idx", [](const ROOT::VecOps::RVec<float>& trkjet_muon0_dR)->uint32_t {
						       return std::distance(trkjet_muon0_dR.begin(),std::min_element(trkjet_muon0_dR.begin(),trkjet_muon0_dR.end()));
						     }, {"trkjet_muon0_dR"})
						   .Define("muon0_trkjet_R" , define_float_ridx, {"trkjet_R", "muon0_trkjet_idx"})
						   .Define("muon0_trkjet_dR", define_float_ridx, {"trkjet_muon0_dR", "muon0_trkjet_idx"})
						   .Define("muon0_trkjet_pt", define_float_idx , {"trkjet_pt", "muon0_trkjet_idx"})
						   .Define("muon0_trkjet_numConstituents", define_int32_idx, {"trkjet_numConstituents", "muon0_trkjet_idx"})
						   .Define("muon0_trkjet_HadronConeExclTruthLabelID", define_int32_idx, {"trkjet_HadronConeExclTruthLabelID", "muon0_trkjet_idx"})
						   );

  // tag fatjets
  df_tag_fatjet = std::make_shared<ROOT::RDF::RNode> ((*df_muon_pt)
						      .Define("tag_fatjet_idx", CRTTbar::tag_fatjet_idx,
							      {"muon_p4", "fatjet" + syst + "_p4"})
						      .Filter([](uint32_t tag_fatjet_idx)->bool { return tag_fatjet_idx != 666; }, {"tag_fatjet_idx"})
						      .Define("tagjet_trkjet_idx", CRTTbar::tag_vrjets,
							      {"tag_fatjet_idx", "fatjet" + syst + "_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_pt", "trkjet_R", "trkjet_muon0_dR"})
						      .Define("tagjet_ntrkjet", vector_size<uint32_t>, {"tagjet_trkjet_idx"})
						      .Filter([](uint32_t ntrkjet)->bool { return ntrkjet>=1; }, {"tagjet_ntrkjet"})
						      .Define("tagjet_nbtag", CRTTbar::tagjet_nbtag,
							      {"tagjet_trkjet_idx", "trkjet_btag"} )
						      .Filter([](uint32_t nbtag)->bool { return nbtag == 1; }, {"tagjet_nbtag"}, "tagjet_nbtag"));

  // probe fatjets
  df_probe_fatjet = std::make_shared<ROOT::RDF::RNode> ((*df_tag_fatjet)
							.Define("Hcand_idx", CRTTbar::probe_fatjet_idx, {"tag_fatjet_idx"})
							.Filter([](uint32_t probe_fatjet_idx)->bool { return probe_fatjet_idx != 666; }, {"Hcand_idx"})
							.Define("probejet_trkjet_idx", CRTTbar::probe_vrjets,
								{"Hcand_idx", "fatjet" + syst + "_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_pt"})
							.Define("probejet_ntrkjet", vector_size<uint32_t>, {"probejet_trkjet_idx"})
							.Filter([](uint32_t ntrkjet)->bool { return ntrkjet>=3; }, {"probejet_ntrkjet"})
							.Define("probejet_nbtag", CRTTbar::probejet_nbtag,
								{"probejet_trkjet_idx", "trkjet_btag"} )
							.Filter([](uint32_t nbtag)->bool { return nbtag == 1; }, {"probejet_nbtag"})
							);

  // hemisphere requirement
  df_hemisphere = std::make_shared<ROOT::RDF::RNode> ((*df_probe_fatjet)
						      .Filter(CRTTbar::filter_probe_tag_deltaPhi, {"tag_fatjet_idx", "Hcand_idx", "fatjet" + syst + "_p4"}));

  // CRttbar
  if(m_isMC)
    {
      df_crttbar = std::make_shared<ROOT::RDF::RNode> ((*df_hemisphere)
						       .Define("poly_reweight", CRTTbar::get_poly_reweight,
							       {"fatjet" + syst + "_pt", "Hcand_idx"}));

      //
      // b-tagging systematics loop
      const std::vector<std::string> the_btagging_systematics=( syst.empty() && btagging_systematics.count(btagWP)!=0 ) ? btagging_systematics.at(btagWP) : std::vector<std::string>({""});

      for(uint32_t btag_syst_idx=0;
	  btag_syst_idx<the_btagging_systematics.size();
	  btag_syst_idx++)
	{
	  std::string bsystname=the_btagging_systematics[btag_syst_idx];

	  df_crttbar=std::make_shared<ROOT::RDF::RNode>((*df_crttbar)
							.Define("w"+bsystname, [btag_syst_idx](float weight, float poly_reweight, const std::vector<uint32_t>& tagjet_trkjet_idx, const std::vector<uint32_t>& probejet_trkjet_idx, const std::vector<std::vector<float>>& trkjet_SF)->float
								{
								  return weight*poly_reweight*CRTTbar::btagSF(tagjet_trkjet_idx,probejet_trkjet_idx,trkjet_SF,btag_syst_idx);
								}, {"weight_trigger", "poly_reweight", "tagjet_trkjet_idx", "probejet_trkjet_idx","trkjet_SF_"+btagWP})
							);
	}

      df_crttbar = Define_Weights(df_crttbar);
    }
  else
    {
      df_crttbar = std::make_shared<ROOT::RDF::RNode> ((*df_hemisphere)
						       .Define("w", []()->float { return 1.; })
						       .Filter(CRTTbar::random_toss, {"fatjet" + syst + "_pt", "Hcand_idx"})
						       );
    }

  // other useful defines
  df_crttbar = std::make_shared<ROOT::RDF::RNode> ((*df_crttbar)
						   .Define("Hcand_pt", define_float_idx,
							   {"fatjet" + syst + "_muonCorrected_pt", "Hcand_idx"})
						   .Define("Hcand_uncorr_pt",
							   define_float_idx,
							   {"fatjet" + syst + "_pt", "Hcand_idx"})
						   .Define("Hcand_m",
							   define_float_idx,
							   {"fatjet" + syst + "_muonCorrected_m", "Hcand_idx"})
						   .Define("Hcand_uncorr_m",
							   define_float_idx,
							   {"fatjet" + syst + "_m", "Hcand_idx"})
						   .Define("tag_fatjet_pt",
							   define_float_idx,
							   {"fatjet" + syst + "_muonCorrected_pt", "tag_fatjet_idx"})
						   .Define("tag_fatjet_uncorr_pt",
							   define_float_idx,
							   {"fatjet" + syst + "_pt", "tag_fatjet_idx"})
						   .Define("tag_fatjet_m",
							   define_float_idx,
							   {"fatjet" + syst + "_muonCorrected_m", "tag_fatjet_idx"})
						   .Define("tag_fatjet_uncorr_m",
							   define_float_idx,
							   {"fatjet" + syst + "_m", "tag_fatjet_idx"})
						   );

  if(!m_reweightMapName.empty())
    {
      df_crttbar = std::make_shared<ROOT::RDF::RNode> ((*df_crttbar)
						       .Define("Hcand_eta", define_float_idx, {"fatjet" + syst + "_eta", "Hcand_idx"})
						       .Define("Hcand_phi", define_float_idx, {"fatjet" + syst + "_phi", "Hcand_idx"})
						       .Define("truthjet_p4", PtEtaPhiM,
							       {"ntruthfatjet", "truthfatjet_pt", "truthfatjet_eta", "truthfatjet_phi", "truthfatjet_m"})
						       .Define("Hcand_truth_idx" , matchdR_idx, {"Hcand_pt","Hcand_eta","Hcand_phi","Hcand_m","truthjet_p4"})
						       .Define("Hcand_truth_pt"  , define_float_idx, {"truthfatjet_pt","Hcand_truth_idx"})
						       .Define("Hcand_truth_m"   , define_float_idx, {"truthfatjet_m" ,"Hcand_truth_idx"})
						       .Define("reweight"        , [this](float Hcand_truth_m, float Hcand_truth_pt)->float {return getReweight(this->m_sysMaps["crttbar"],Hcand_truth_m,Hcand_truth_pt);} , {"Hcand_truth_m","Hcand_truth_pt"})
						       );
    }

}

CRTTbarAnalysis::~CRTTbarAnalysis()
{
  // Close configuration files
  if(m_fMaps)
    {
      m_fMaps->Close();
      m_fMaps=nullptr;
    }

  if(m_fEW)
    {
      m_fEW->Close();
      m_fEW=nullptr;
    }
}

std::shared_ptr<ROOT::RDF::RNode> CRTTbarAnalysis::Define_Weights(std::shared_ptr<ROOT::RDF::RNode> df)
{
  for(const std::string& wname : extra_weights)
    {
      const std::unordered_map<uint32_t, uint32_t>& systidxmap=m_weightdb.systIdx(wname);

      df=std::make_shared<ROOT::RDF::RNode>((*df)
					    .Define("w"+wname, [this,systidxmap](int runNumber, int mcChannelNumber, float weight, const std::vector<float>& mcEventWeights)->float {
						if(systidxmap.count(mcChannelNumber)==1 && this->m_SumW.count(dskey(runNumber,mcChannelNumber))==1)
						  {
						    uint32_t systidx=systidxmap.at(mcChannelNumber);
						    const std::vector<double>& sumw=this->m_SumW.at(dskey(runNumber,mcChannelNumber));
						    return weight*
						      (mcEventWeights.at(systidx)/sumw.at(systidx))/
						      (mcEventWeights.at(      0)/sumw.at(      0));
						  }
						else
						  return weight;
					      },
					      {"runNumber", "mcChannelNumber", "w", "mcEventWeights"}
					      )
					    );
    }

  //
  // Remove EW corrections
  df=std::make_shared<ROOT::RDF::RNode>((*df)
					.Define("wNOEW", [](float weight, float ewcorr)->float
						{ return weight/ewcorr; },
						{"w", "ewcorr"}
						)
					);

  return df;
}

void CRTTbarAnalysis::LoadReweightMaps()
{
  // Load 2D maps
  if(!m_reweightMapName.empty())
    {
      m_fMaps = TFile::Open(m_reweightMapName.c_str());
      std::vector<std::string> regions = {"crttbar"};
      for(std::string reg : regions)
        {
         std::string mapName = "hMap_"+reg;
         m_sysMaps.insert(std::pair<std::string, TH2*>(reg, m_fMaps->Get<TH2>(mapName.data())));
        }
    }

  // Load EW corrections
  if(!m_ewCorections.empty())
    {
      m_fEW=TFile::Open(m_ewCorections.c_str());

      m_EW_WmH =m_fEW->Get<TF1>("WmH" );
      m_EW_WpH =m_fEW->Get<TF1>("WpH" );
      m_EW_ZllH=m_fEW->Get<TF1>("ZHll");
      m_EW_ZvvH=m_fEW->Get<TF1>("ZHvv");
      m_EW_VH  =m_fEW->Get<TF1>("VH"  );
      m_EW_VBF =m_fEW->Get<TF1>("VBF" );
      m_EW_ttH =m_fEW->Get<TF1>("ttH" );
    }
}
