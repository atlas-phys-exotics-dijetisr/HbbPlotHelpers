#include "Defines.h"

ROOT::VecOps::RVec<TLorentzVector> PtEtaPhiM(int32_t n, const std::vector<float>& pt, const std::vector<float>& eta, const std::vector<float>& phi, const std::vector<float>& m)
{
  ROOT::VecOps::RVec<TLorentzVector> p4(n);
  for(int32_t i=0;i<n;i++)
    p4[i].SetPtEtaPhiM(pt[i],eta[i],phi[i],m[i]);
  return p4;
}

ROOT::VecOps::RVec<TLorentzVector> PtEtaPhiE(int32_t n, const std::vector<float>& pt, const std::vector<float>& eta, const std::vector<float>& phi, const std::vector<float>& e)
{
  ROOT::VecOps::RVec<TLorentzVector> p4(n);
  for(int32_t i=0;i<n;i++)
    p4[i].SetPtEtaPhiE(pt[i],eta[i],phi[i],e[i]);
  return p4;
}

std::vector<float> GetMass(const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4)
{
  std::vector<float> M(fatjet_p4.size());
  std::transform(fatjet_p4.begin(), fatjet_p4.end(), M.begin(), [](TLorentzVector p4) { return p4.M(); });
  return M;
}

uint32_t matchdR_idx(float pt, float eta, float phi, float m, const ROOT::VecOps::RVec<TLorentzVector>& truthjet_p4)
{ 
  TLorentzVector fatjet_p4;
  fatjet_p4.SetPtEtaPhiM(pt,eta,phi,m);
  std::vector<float> dR(truthjet_p4.size());
  std::transform(truthjet_p4.begin(), truthjet_p4.end(), dR.begin(), [&fatjet_p4](TLorentzVector truth_p4){ return truth_p4.DeltaR(fatjet_p4); });
  std::vector<float>::iterator it = std::min_element(dR.begin(), dR.end());
  return (uint32_t)std::distance(dR.begin(), it);
}

float getReweight(TH2* hMap, float truth_m, float truth_pt)
{
  int xBin = hMap->GetXaxis()->FindBin(truth_m);
  if(xBin<=0) xBin=1;
  if(xBin>hMap->GetXaxis()->GetNbins()) xBin=hMap->GetXaxis()->GetNbins();
  int yBin = hMap->GetYaxis()->FindBin(truth_pt);
  if(yBin<=0) yBin=1;
  if(yBin>hMap->GetYaxis()->GetNbins()) yBin=hMap->GetYaxis()->GetNbins();

  return hMap->GetBinContent(xBin,yBin);
}

std::vector<int> CountHadrons(const ROOT::VecOps::RVec<TLorentzVector>& jet_p4,
                              const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4,
                              const ROOT::VecOps::RVec<int>& jet_GhostHadronsFinalCount)
{
  std::vector<int> fatjet_nHadrons;
  for(const TLorentzVector& fatjet : fatjet_p4){
    int nHad    = 0;
    int jet_idx = 0;
    for(const TLorentzVector& jet : jet_p4){
      if(fatjet.DeltaR(jet)<1.0) nHad+=jet_GhostHadronsFinalCount[jet_idx];
      jet_idx++;
      }
    fatjet_nHadrons.push_back(nHad);
    }
  return fatjet_nHadrons;
}


bool filter_flag(bool flag)
{ return flag; }

bool filter_or(bool trigger0, bool trigger1, bool trigger2, bool trigger3)
{ return trigger0 || trigger1 || trigger2 || trigger3; }

bool filter_trigger_or1(int8_t trigger0)
{ return (trigger0==1); }


bool filter_trigger_or4(int8_t trigger0, int8_t trigger1, int8_t trigger2, int8_t trigger3)
{ return (trigger0==1) || (trigger1==1) || (trigger2==1) || (trigger3==1); }

bool trigger_jet_ptmass(const std::vector<float>& fatjet_pt, const std::vector<float>& fatjet_m, float ptcut, float mcut)
{
  for(uint32_t i=0;i<fatjet_pt.size();i++)
    {
      if(fatjet_pt[i]>ptcut && fatjet_m[i]>mcut) return true;
    }
  return false;
}

float define_float_idx (const std::vector<float>& vec, uint32_t idx)
{ return vec[idx]; }

float define_float_idx0(const std::vector<float>& vec)
{ return vec[0]; }

float define_float_ridx(const ROOT::VecOps::RVec<float>& vec, uint32_t idx)
{ return vec[idx]; }

uint32_t define_uint32_ridx(const ROOT::VecOps::RVec<uint32_t>& vec, uint32_t idx)
{ return vec[idx]; }

int32_t define_int32_idx (const std::vector<int32_t>& vec, uint32_t idx)
{ return vec[idx]; }

int32_t define_int32_ridx(const ROOT::VecOps::RVec<int32_t>& vec, uint32_t idx)
{ return vec[idx]; }

float DeltaR(float eta0, float phi0, float eta1, float phi1)
{ return sqrt(pow(eta0-eta1,2)+pow(TVector2::Phi_mpi_pi(phi0-phi1),2)); }

float DeltaPhi(float phi0, float phi1)
{ return fabs(TVector2::Phi_mpi_pi(phi0-phi1)); }

ROOT::VecOps::RVec<uint32_t> indexlist(int32_t n)
{
  ROOT::VecOps::RVec<uint32_t> idx(n);
  std::iota(idx.begin(),idx.end(),0);
  return idx;
}

ROOT::VecOps::RVec<float> fatjet_boost(const std::vector<float>& trkjet_pt,const std::vector<float>& trkjet_m)
{
  ROOT::VecOps::RVec<float> boost(trkjet_pt.size());
  for(uint32_t i=0;i<boost.size();i++)
    boost[i]=2*trkjet_m[i]/trkjet_pt[i];
  return boost;
}

ROOT::VecOps::RVec<float> trackjet_VR(const std::vector<float>& trkjet_pt)
{
  ROOT::VecOps::RVec<float> R(trkjet_pt.size());
  std::transform(trkjet_pt.begin(), trkjet_pt.end(), R.begin(), [](float pt) { return std::max<float>(0.02, std::min<float>(0.4, 30/pt)); });
  return R;
}

float trkjet_VR(float trkjet_pt)
{
  return std::max<float>(0.02, std::min<float>(0.4, 30/trkjet_pt));
}

uint32_t fatjet_ntrkjet(uint32_t idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs)
{
  return fatjet_trkjet_idxs[idx].size();
}

bool fatjet_trkjet_olap(uint32_t fatjet_idx, uint32_t trkjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs,
			const std::vector<float>& trkjet_pt, const std::vector<float>& trkjet_eta, const std::vector<float>& trkjet_phi)
{
  const std::vector<uint32_t>& fatjet_trkjet_idx=fatjet_trkjet_idxs[fatjet_idx];

  uint32_t iidx=fatjet_trkjet_idx[trkjet_idx];
  float R0=trkjet_VR(trkjet_pt[iidx]);

  for(uint32_t j=0;j<fatjet_trkjet_idx.size();j++)
    {
      if(j==trkjet_idx) continue;
      uint32_t jidx=fatjet_trkjet_idx[j];

      float R1=trkjet_VR(trkjet_pt[jidx]);
      float minR=std::min(R0,R1);
      float dR=DeltaR(trkjet_eta[iidx],trkjet_eta[jidx],
		      trkjet_phi[iidx],trkjet_phi[jidx]);

      if(dR/minR<1) return true;
    }
  return false;
}


ROOT::VecOps::RVec<bool> fatjet_VR_overlap(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R)
{
  ROOT::VecOps::RVec<bool> VR_overlap(fatjet_trkjet_idxs.size());
  std::transform(fatjet_trkjet_idxs.begin(), fatjet_trkjet_idxs.end(), VR_overlap.begin(), [&trkjet_p4,&trkjet_R](const std::vector<uint32_t>& idx) {
      for(uint32_t i=0;i<std::min<uint32_t>(2,idx.size());i++)
	{
	  uint32_t iidx=idx[i];
	  if(trkjet_p4[idx[i]].Pt()<10) break;
	  float R0=trkjet_R[idx[i]];
	  for(uint32_t j=i;j<idx.size();j++)
            {
	      if(i==j) continue;
              float R1=trkjet_R[idx[j]];
              float minR=std::min(R0,R1);
              float dR=trkjet_p4[idx[i]].DeltaR(trkjet_p4[idx[j]]);
	      if(dR/minR<1) return true;
            }
	}
      return false;
    });
  return VR_overlap;
}

ROOT::VecOps::RVec<bool> fatjet_VR_overlapN(uint32_t N, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R)
{
  ROOT::VecOps::RVec<bool> VR_overlap(fatjet_trkjet_idxs.size());
  std::transform(fatjet_trkjet_idxs.begin(), fatjet_trkjet_idxs.end(), VR_overlap.begin(), [N,&trkjet_p4,&trkjet_R](const std::vector<uint32_t>& idx) {
      for(uint32_t i=0;i<std::min<uint32_t>(N,idx.size());i++)
	{
	  uint32_t iidx=idx[i];
	  if(trkjet_p4[idx[i]].Pt()<10) break;
	  float R0=trkjet_R[idx[i]];
	  for(uint32_t j=i;j<idx.size();j++)
            {
	      if(i==j) continue;
              float R1=trkjet_R[idx[j]];
              float minR=std::min(R0,R1);
              float dR=trkjet_p4[idx[i]].DeltaR(trkjet_p4[idx[j]]);
	      if(dR/minR<1) return true;
            }
	}
      return false;
    });
  return VR_overlap;
}

ROOT::VecOps::RVec<bool> fatjet_VR_overlap2(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R)
{ return fatjet_VR_overlapN(2, fatjet_trkjet_idxs, trkjet_p4, trkjet_R); }

ROOT::VecOps::RVec<bool> fatjet_VR_overlap3(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R)
{ return fatjet_VR_overlapN(3, fatjet_trkjet_idxs, trkjet_p4, trkjet_R); }


std::vector<std::vector<uint32_t>> fatjet_trkjet5_idxs (const std::vector<std::vector<uint32_t>>& idxs, const std::vector<float>& trkjet_pt)
{
  std::vector<std::vector<uint32_t>> out(idxs.size());
  std::transform(idxs.begin(),idxs.end(),out.begin(), [&trkjet_pt](const std::vector<uint32_t>& idx)
		 {
		   std::vector<uint32_t> outidx;
		   std::copy_if(idx.begin(), idx.end(),std::back_inserter(outidx), [&trkjet_pt](uint32_t trkidx)
				{
				  return trkjet_pt[trkidx]>5. ;
				});
		   return outidx;
		 });
  return out;
}

std::vector<std::vector<uint32_t>> fatjet_trkjet10_idxs(const std::vector<std::vector<uint32_t>>& idxs, const std::vector<float>& trkjet_pt)
{
  std::vector<std::vector<uint32_t>> out(idxs.size());
  std::transform(idxs.begin(),idxs.end(),out.begin(), [&trkjet_pt](const std::vector<uint32_t>& idx)
		 {
		   std::vector<uint32_t> outidx;
		   std::copy_if(idx.begin(), idx.end(),std::back_inserter(outidx), [&trkjet_pt](uint32_t trkidx)
				{
				  return trkjet_pt[trkidx]>10.;
				});
		   return outidx;
		 });
  return out;
}

ROOT::VecOps::RVec<uint32_t> count_subjet(const std::vector<std::vector<uint32_t>>& idxs)
{
  ROOT::VecOps::RVec<uint32_t> out(idxs.size());
  std::transform(idxs.begin(),idxs.end(),out.begin(), [](const std::vector<uint32_t>& idx)->uint32_t {return idx.size();});
  return out;
}

ROOT::VecOps::RVec<uint32_t> count_bjetsin2(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int>& trkjet_btag)
{
  ROOT::VecOps::RVec<uint32_t> out(fatjet_trkjet_idxs.size());
  std::transform(fatjet_trkjet_idxs.begin(),fatjet_trkjet_idxs.end(),out.begin(), [&trkjet_btag](const std::vector<uint32_t>& idx) {
      uint32_t nbtag=0;
      for(uint i=0;i<std::min<uint32_t>(2,idx.size());i++)
	{
	  nbtag+=trkjet_btag[idx[i]];
	}
      return nbtag;
    });
  return out;
}

ROOT::VecOps::RVec<uint32_t> count_bjetsin3(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int>& trkjet_btag)
{ return count_bjetsinN(3, fatjet_trkjet_idxs, trkjet_btag); }

ROOT::VecOps::RVec<uint32_t> count_bjetsinN(uint32_t N, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int>& trkjet_btag)
{
  ROOT::VecOps::RVec<uint32_t> out(fatjet_trkjet_idxs.size());
  std::transform(fatjet_trkjet_idxs.begin(),fatjet_trkjet_idxs.end(),out.begin(), [N,&trkjet_btag](const std::vector<uint32_t>& idx) {
      uint32_t nbtag=0;
      for(uint i=0;i<std::min<uint32_t>(N,idx.size());i++)
	{
	  nbtag+=trkjet_btag[idx[i]];
	}
      return nbtag;
    });
  return out;
}

ROOT::VecOps::RVec<uint32_t> count_truthjets(int32_t truthid, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)
{
  ROOT::VecOps::RVec<uint32_t> out(fatjet_trkjet_idxs.size());
  std::transform(fatjet_trkjet_idxs.begin(),fatjet_trkjet_idxs.end(),out.begin(), [&truthid,&trkjet_truth](const std::vector<uint32_t>& idx) {
      uint32_t n=0;
      for(uint i=0;i<std::min<uint32_t>(2,idx.size());i++)
	n+=(trkjet_truth[idx[i]]==truthid);
      return n;
    });
  return out;
}


///////////////////////////// ttbar CR functions ////////////////////////////////////////////
// returns T/F for each trkjet if it has an overlap
// if pT < 5 GeV, it's considered overlapped (for btagging)
ROOT::VecOps::RVec<bool> trkjet_overlap(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R)
{
  // Loop over the trackjet idxs grouped in vectors for each fatjet
  ROOT::VecOps::RVec<bool> out(trkjet_p4.size(), true);

  // Loop over largeR jets to only check overlap within jet
  // on filtered track jets
  for(const std::vector<uint32_t>& fatjet_trkjet_idx : fatjet_trkjet_idxs)
    {
      // loop over the trackjet idxs
      for(const uint32_t tjidx0 : fatjet_trkjet_idx)
	{
	  out[tjidx0]=false;
	  const TLorentzVector& trkjet0 = trkjet_p4[tjidx0];

	  // trackjet needs a pt > 10.0
	  if (trkjet0.Pt() < 10.0)
	    {
	      out[tjidx0]=true;
	      continue;
	    }

	  // loop over the other trkjet p4s
	  for(const uint32_t tjidx1 : fatjet_trkjet_idx)
	    {
	      if (tjidx0 == tjidx1) continue;
	      // other track jet
	      const TLorentzVector& trkjet1 = trkjet_p4[tjidx1];
	      if (trkjet1.Pt() < 5.0) continue; // needs to have a pT > 5 GeV

	      // other jet R, dR between them, and the min of the two
	      float dR    = trkjet0.DeltaR(trkjet1);
	      float minR  = std::min(trkjet_R[tjidx0], trkjet_R[tjidx1]);
	      if (dR/minR < 1.0)
		{
		  out[tjidx0] = true;
		  break; // only need one bad apple
		}
	    }
	}
    }
  return out;
}

// returns T/F for each b-tagged fatjet
// needs to have a pT > 10 and no overlap
ROOT::VecOps::RVec<bool> trkjet_btagged(const std::vector<int>& trkjet_btag, const ROOT::VecOps::RVec<bool>& trkjet_overlap)
{
  ROOT::VecOps::RVec<bool> out(trkjet_btag.size(), false);

  // loop over the trackjets
  for(uint32_t i = 0; i < trkjet_btag.size(); i++)
    out[i] = trkjet_btag[i] && !trkjet_overlap[i]; // btag == btag and not overlapping

  return out;
}
