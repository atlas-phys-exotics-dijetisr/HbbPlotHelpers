#ifndef CRTTBARANALYSIS_H
#define CRTTBARANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>
#include <TH2.h>
#include <TF1.h>
#include <TFile.h>

#include "WeightDB.h"

class CRTTbarAnalysis
{
public:
  /**
   * Construct the selection for the boosted Hbb+jet signal and validation regions
   *
   * \param rootdf base RDF node on which to start the selection
   * \param isMC indicate whether running over MC (true) or data (false)
   * \param syst suffix of a fat jet systematic, leave blank for nominal
   * \param btagWP b-tagging working point to use
   * \param br_truth_pt input branch to save into the output "truth_pt" column
   * \param SumW The key is the sample DSID, value is a list with each element being the corresponding weight's sumW.
   * \param reweightMapName Name of the file containing the reweighting map
   * \param ewCorrections Name of the file containing the EW corrections for Higgs
   */
  CRTTbarAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false, const std::string& syst="", const std::string& br_truth_pt="Higgs_pt", const std::string& btagWP="MV2c10_FixedCutBEff_77", const std::unordered_map<uint32_t, std::vector<double>>& SumW={}, const std::string& reweightMapName = "", const std::string& ewCorrections = "");
  ~CRTTbarAnalysis();

  std::vector<std::string> extra_weights={"MUR0p5_MUF0p5","MUR0p5_MUF1p0","MUR1p0_MUF0p5","MUR1p0_MUF1p0","MUR1p0_MUF2p0","MUR2p0_MUF1p0","MUR2p0_MUF2p0","Var3cUp","Var3cDown","isr_muRfac1p0_fsr_muRfac2p0","isr_muRfac1p0_fsr_muRfac0p5"};
  
  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  // filtered for events w/ at least a muon and two fatjets w/ pt > 250
  std::shared_ptr<ROOT::RDF::RNode> df_filtered;
  // four-momenta
  std::shared_ptr<ROOT::RDF::RNode> df_defp4;
  // pile-up reweight
  std::shared_ptr<ROOT::RDF::RNode> df_prw;
  // trigger weight
  std::shared_ptr<ROOT::RDF::RNode> df_trigger;
  // trigger muon match
  std::shared_ptr<ROOT::RDF::RNode> df_triggermuon;
  // jet cleaning
  std::shared_ptr<ROOT::RDF::RNode> df_cleaning;
  // btag for non-overlapping trkjets
  std::shared_ptr<ROOT::RDF::RNode> df_trkjet_btag;
  // high pT muon
  std::shared_ptr<ROOT::RDF::RNode> df_muon_pt;
  // Tag fatjet
  std::shared_ptr<ROOT::RDF::RNode> df_tag_fatjet;
  // Tag probe_jet
  std::shared_ptr<ROOT::RDF::RNode> df_probe_fatjet;
  // dPhi checks
  std::shared_ptr<ROOT::RDF::RNode> df_hemisphere;
  // Product of scale factors
  std::shared_ptr<ROOT::RDF::RNode> df_scale_factors;
  // CRttbar
  std::shared_ptr<ROOT::RDF::RNode> df_crttbar;

  // Systematic Variation Maps
  std::vector<TH2*> sysMaps;
  void GetMaps();

private:
  std::shared_ptr<ROOT::RDF::RNode> Define_Weights(std::shared_ptr<ROOT::RDF::RNode> df);

  bool m_isMC = false;

  std::unordered_map<uint32_t, std::vector<double>> m_SumW = {};
  WeightDB m_weightdb;

  //
  // Reweighting support

  //! Path to file with 2D reweighting maps
  const std::string m_reweightMapName;
  TFile* m_fMaps=nullptr;

  std::map<std::string,TH2*> m_sysMaps;
  
  void LoadReweightMaps();

  //
  // EW corrections

  //! Path to file with EW corrections
  const std::string m_ewCorections;

  TFile* m_fEW =nullptr;

  TF1* m_EW_WpH =nullptr;
  TF1* m_EW_WmH =nullptr;
  TF1* m_EW_ZllH=nullptr;
  TF1* m_EW_ZvvH=nullptr;
  TF1* m_EW_VH  =nullptr;
  TF1* m_EW_VBF =nullptr;
  TF1* m_EW_ttH =nullptr;
};

#endif // CRTTBARANALYSIS_H
