# Introduction
Framework for processing ntuples using the RDataFrame framework in ROOT. Meant to be used with the Hbb+jet analysis.

**Warning:** Currently at a prototype stage. API might change often!

# Requirements
- ROOT 6.18/00 or later
- CMake 3
- yaml-cpp 0.6 or later

# Installation
Building of the framework is managed using CMake.
```sh
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-exotics-dijetisr/HbbPlotHelpers.git
cd HbbPlotHelpers
mkdir build
cd build
cmake ..
make
```

## Installing Dependencies Using LCGenv on CVMFS (ie: lxplus)
LCGenv has all the necessary dependencies and is readily available on most clusters, including lxplus. To enable them, run the following everytime on login.

```
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=$HOME/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

lsetup "lcgenv -p LCG_96bpython3 x86_64-centos7-gcc8-opt yamlcpp"
lsetup "lcgenv -p LCG_96bpython3 x86_64-centos7-gcc8-opt ROOT"
```

A few extra paths have to be given to CMake to find everything.
```
cmake3 .. -Dyaml-cpp_DIR=/cvmfs/sft.cern.ch/lcg/releases/LCG_96bpython3/yamlcpp/0.6.2/x86_64-centos7-gcc8-opt/lib/cmake/yaml-cpp -DCMAKE_CXX_FLAGS="-I/cvmfs/sft.cern.ch/lcg/releases/LCG_96bpython3/vdt/0.4.3/x86_64-centos7-gcc8-opt/include"
```

## Installing Dependencies Using Anaconda
Most clusters support Anaconda for installing dependencies in user environments. This is the best way to get latest version of packages on most clusters, including lxplus.

```
conda create -n myenv
conda activate myenv
conda install -c conda-forge root yaml-cpp
```

Then on every login, run the following to setup the environment.
```
conda activate myenv
```

# Running the Hbb+jet Analysis
The selection used by the Hbb+jet analysis is implemented in `src/EventWideHbbAnalysis.cpp` 
and can be executing using either the `src/testEventWide.cpp` or `src/treeEventWide.cpp` program.
The former creates histograms while the latter creates mini-ntuples.

Running it from the build directory:
```sh
[kkrizka@lxplus757 build]$ ./src/treeEventWide 
usage: ./src/treeEventWide nthreads outDir [sample [systematic [selection]]]
```

The arguments are as follows:
```
nthreads - number of threads to use
outDir - output directory to store the mini-ntuples in
sample - sample to process, can be a comma separated list (default: all nominal samples)
systematic - systematic to process, can be a comma separated list (default: nominal)
selection - selection to execute (default: all of inclusive srl, srs, vrl, vrs)
```

The resulting `TFile`'s can be merged into the standard format using the `scripts/mergeTree.py`.

# Filelists
The `filelists/` directory contains the filelists for all of the samples used by
the Hbb+jet analysis. The files that it points to are stored on eosatlas and can
be accessed on lxplus. It is also possible to create a local filelist using the
following command, where TAG is the latest ntuple processing. 

```sh
rucio download 'user.kkrizka.*TAG_tree.root'
scripts/makeFilelists.py
```

The latest tag can be extracted from the files located on EOS.

# Batch Running
Batch running is implemented by using separte jobs to process a sample/systematic/selection combination
per job. Each job runs in multi-threaded mode, currently fixed to 8 threads.

The first step is to generate the list of commands, one per combination, to run. They need to
be saved into a file called `tasks`. Each command will be executed on an allocated node.
```
../scripts/generateTasks.sh -t > tasks
```

The `-t` switch causes the output to be mini-ntuples instead of histograms.

## Condor (lxbatch)
To run on condor, submit using the following command. The `tasklist` should be replaced by the list of tasks
you want to run. If not specified, it will look for a file called `tasks`.
```
TASKS=tasklist condor_submit ../scripts/condor.sub
```

It might be necessary to edit the `scripts/condor.sh` file to change the path to your build directory.


# Contributors
Karol Krizka <kkrizka@cern.ch>
