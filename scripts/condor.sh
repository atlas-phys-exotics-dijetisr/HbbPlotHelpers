#!/bin/bash

uname -a
pwd

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=${HOME}/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --

lsetup "lcgenv -p LCG_96bpython3 x86_64-centos7-gcc8-opt yamlcpp"
lsetup "lcgenv -p LCG_96bpython3 x86_64-centos7-gcc8-opt ROOT"

cd /afs/cern.ch/user/k/kkrizka/HbbPlotHelpers/build
echo ${@}
eval ${@}

