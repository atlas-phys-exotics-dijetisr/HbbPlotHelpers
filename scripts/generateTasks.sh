#!/bin/bash

#
# Define all the things to run over
systematics=(JET_CombMass_Baseline__1up JET_CombMass_Baseline__1down JET_CombMass_Modelling__1up JET_CombMass_Modelling__1down JET_CombMass_TotalStat__1up JET_CombMass_TotalStat__1down JET_CombMass_Tracking1__1up JET_CombMass_Tracking1__1down JET_CombMass_Tracking2__1up JET_CombMass_Tracking2__1down JET_CombMass_Tracking3__1up JET_CombMass_Tracking3__1down JET_EffectiveNP_R10_Detector1__1up JET_EffectiveNP_R10_Detector1__1down JET_EffectiveNP_R10_Detector2__1up JET_EffectiveNP_R10_Detector2__1down JET_EffectiveNP_R10_Mixed1__1up JET_EffectiveNP_R10_Mixed1__1down JET_EffectiveNP_R10_Mixed2__1up JET_EffectiveNP_R10_Mixed2__1down JET_EffectiveNP_R10_Mixed3__1up JET_EffectiveNP_R10_Mixed3__1down JET_EffectiveNP_R10_Mixed4__1up JET_EffectiveNP_R10_Mixed4__1down JET_EffectiveNP_R10_Modelling1__1up JET_EffectiveNP_R10_Modelling1__1down JET_EffectiveNP_R10_Modelling2__1up JET_EffectiveNP_R10_Modelling2__1down JET_EffectiveNP_R10_Modelling3__1up JET_EffectiveNP_R10_Modelling3__1down JET_EffectiveNP_R10_Modelling4__1up JET_EffectiveNP_R10_Modelling4__1down JET_EffectiveNP_R10_Statistical1__1up JET_EffectiveNP_R10_Statistical1__1down JET_EffectiveNP_R10_Statistical2__1up JET_EffectiveNP_R10_Statistical2__1down JET_EffectiveNP_R10_Statistical3__1up JET_EffectiveNP_R10_Statistical3__1down JET_EffectiveNP_R10_Statistical4__1up JET_EffectiveNP_R10_Statistical4__1down JET_EffectiveNP_R10_Statistical5__1up JET_EffectiveNP_R10_Statistical5__1down JET_EffectiveNP_R10_Statistical6__1up JET_EffectiveNP_R10_Statistical6__1down JET_EtaIntercalibration_Modelling__1up JET_EtaIntercalibration_Modelling__1down JET_EtaIntercalibration_NonClosure_2018data__1up JET_EtaIntercalibration_NonClosure_2018data__1down JET_EtaIntercalibration_R10_TotalStat__1up JET_EtaIntercalibration_R10_TotalStat__1down JET_Flavor_Composition__1up JET_Flavor_Composition__1down JET_Flavor_Response__1up JET_Flavor_Response__1down JET_LargeR_TopologyUncertainty_V__1up JET_LargeR_TopologyUncertainty_V__1down JET_LargeR_TopologyUncertainty_top__1up JET_LargeR_TopologyUncertainty_top__1down JET_MassRes_Hbb_comb__1up JET_MassRes_Hbb_comb__1down JET_MassRes_Top_comb__1up JET_MassRes_Top_comb__1down JET_MassRes_WZ_comb__1up JET_MassRes_WZ_comb__1down JET_SingleParticle_HighPt__1up JET_SingleParticle_HighPt__1down)

signals=(Higgs_ggf Higgs_VBF Higgs_VH Higgs_ttH Sherpa_Wqq Sherpa_Zqq Herwig_Wqq Herwig_Zqq ttbar_PowPy8 ttbar_PowHer ttbar_MG5Py8 Powheg_singletop Sherpa_Wlnu)
backgrounds=(data Pythia8_dijet Sherpa_ttbar Sherpa225_Wqq Sherpa225_Zqq)

regions=(srl srs vrl vrs)

#
# parse any arguments
function print_usage
{
    echo "usage: ${0} [-h]"
    echo ""
    echo "-h: print help and exit"
    exit 1
}

OPTS="-t 8"
OPTS_SIG="-e ../data/Higgs_EW_corrections.root -b ${OPTS}"
OPTS_BKG="${OPTS}"
OPTS_SYS=" -e ../data/Higgs_EW_corrections.root ${OPTS}"

OUTDIR=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/dibjetISR_boosted/data_2020221/split
while getopts ":h" opt; do
    case ${opt} in
	h )
	    print_usage
	    ;;
	\? )
	    print_usage
	    ;;
    esac
done


#
# Run over everything
for signal in ${signals[@]}
do
    for region in ${regions[@]}
    do
     	echo ./src/treeEventWide ${OPTS_SIG} ${OUTDIR} ${signal} nominal ${region}
    done
    echo ./src/treeCRTTbar ${OPTS_SIG} ${OUTDIR} ${signal} nominal crttbar
done

for signal in ${backgrounds[@]}
do
    for region in ${regions[@]}
    do
     	echo ./src/treeEventWide ${OPTS_BKG} ${OUTDIR} ${signal} nominal ${region}
    done
    echo ./src/treeCRTTbar ${OPTS_BKG} ${OUTDIR} ${signal} nominal crttbar
done

for systematic in "${systematics[@]}"
do
    for signal in ${signals[@]}
    do
	for region in ${regions[@]}
	do
	    echo ./src/treeEventWide ${OPTS_SYS} ${OUTDIR} ${signal} ${systematic} ${region}
	done
	echo ./src/treeCRTTbar ${OPTS_SYS} ${OUTDIR} ${signal} ${systematic} crttbar
    done
done
