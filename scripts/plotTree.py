import ROOT as ROOT
import sys, argparse
from math import sqrt

def make1DPlot(df,hModel,weight,doReweight=False):
  columns = df.GetColumnNames()
  if weight == '':
    df = df.Define('final_weight','1')
  elif weight not in columns:
    print('Error: no valid weight defined.')
    sys.exit()
  else:
    if not doReweight:
      df = df.Define('final_weight',weight)
    else:
      if 'reweight' not in columns:
        print('Error: no reweight variable in dataframe.')
        sys.exit()
      else:
        df = df.Define('final_weight','{}*{}'.format(weight,'reweight'))
  h1D = df.Histo1D(hModel,hModel.fName,'final_weight')
  rwErrInDf = True
  if doReweight and 'reweightErr' not in columns:
    rwErrInDf = False
    print('Warning: no reweightErr variable in dataframe. Histograms will only have statistical error.')
  if doReweight and rwErrInDf:
    df     = df.Define('up_weight','w*(reweight+reweightErr)')
    h1D_up = df.Histo1D(hModel,hModel.fName,'up_weight')
    for i in range(1,h1D.GetXaxis().GetNbins()+1):
      err_stat = h1D.GetBinError(i)
      err_rw   = abs(h1D_up.GetBinContent(i)-h1D.GetBinContent(i))
      err      = sqrt(err_stat**2 + err_rw**2)
      h1D.SetBinError(i,err)
  return h1D

def splitFlavours(df_dict,flavList=['bb','bx','xx']):
  out_dict = {}
  for key in df_dict: 
    df = df_dict[key]
    if 'category' not in df.GetColumnNames():
      return False 
    jet_label = ''
    if any(reg_l in key for reg_l in ['srl','vrl','bbl','bxl','xxl']):
      jet_label = 'l'
    elif any(reg_s in key for reg_s in ['srs','vrs','bbs','bxs','xxs']):
      jet_label = 's'
    else:
      return False
    for flav in flavList:
        out_dict['{}_{}'.format(key,flav)] = df.Filter('category == \"{}{}\"'.format(flav,jet_label))
  return out_dict

def splitPt(df_dict,ptList=[450,650]):
  ptList.sort()
  out_dict = {} 
  for key in df_dict:
    df = df_dict[key]
    if 'Hcand_pt' not in df.GetColumnNames(): 
      return False
    for pt in ptList:
        if pt == ptList[0]:
          out_dict['{}_pt_l{}'.format(key,str(pt))] = df.Filter('Hcand_pt<{}'.format(pt))
        if pt != ptList[-1]:
          pt_idx = ptList.index(pt)
          pt1 = pt
          pt2 = ptList[pt_idx+1]
          out_dict['{}_pt_g{}l{}'.format(key,str(pt1),str(pt2))] = df.Filter('Hcand_pt>{} && Hcand_pt<{}'.format(str(pt1),str(pt2)))
        else:
          out_dict['{}_pt_g{}'.format(key,str(pt))] = df.Filter('Hcand_pt>{}'.format(str(pt))) 
  return out_dict

def splitMass(df_dict,mList=[100,150]):
  mList.sort()
  out_dict = {}
  for key in df_dict:
    df = df_dict[key]
    if 'Hcand_m' not in df.GetColumnNames():
      return False
    for m in mList:
        if m == mList[0]:
          out_dict['{}_m_l{}'.format(key,str(m))] = df.Filter('Hcand_m<{}'.format(m))
        if m != mList[-1]:
          m_idx = mList.index(m)
          m1 = m
          m2 = mList[m_idx+1]
          out_dict['{}_m_g{}l{}'.format(key,str(m1),str(m2))] = df.Filter('Hcand_m>{} && Hcand_m<{}'.format(str(m1),str(m2)))
        else:
          out_dict['{}_m_g{}'.format(key,str(m))] = df.Filter('Hcand_m>{}'.format(str(m)))
  return out_dict

def splitC2(df_dict,c2List=[0.15,0.20,0.25]):
  c2List.sort()
  out_dict = {}
  for key in df_dict:
    df = df_dict[key]
    if 'Hcand_C2' not in df.GetColumnNames():
      return False
    for c2 in c2List:
      out_dict['{}_C2_l{}'.format(key,'{0:.2f}'.format(c2).replace('.',''))] = df.Filter('Hcand_C2<{}'.format(c2))
  return out_dict
  
def main():

  # check python version 
  if not (sys.version_info.major == 3 and sys.version_info.minor >= 7):
    print("This script requires Python 3.7 or higher!")
    print("You are using Python {}.{}.".format(sys.version_info.major, sys.version_info.minor))
    sys.exit()

  # parse arguments
  parser = argparse.ArgumentParser(description='Script used to create root files with plots. The input file must have structure region/tree.')
  parser.add_argument( 'inFileName'     , help='Path to input file.'                                                 )
  parser.add_argument( '-t', '--threads', help='Number of threads to be used.', default = 8    , type=int            )
  parser.add_argument( '--reweight'     , help='Apply reweighting.'           , default = False, action='store_true' )
  parser.add_argument( '--flav'         , help='Split into flavors.'          , default = False, action='store_true' )
  parser.add_argument( '--ptreg'        , help='Split into pT regions.'       , default = False, action='store_true' )
  parser.add_argument( '--mreg'         , help='Split into mass regions.'     , default = False, action='store_true' )
  parser.add_argument( '--c2'           , help='Apply C2 cuts.'               , default = False, action='store_true' )
  args = parser.parse_args()

  # enable multi-threading
  ROOT.ROOT.EnableImplicitMT(args.threads)

  # open input file
  inFile  = ROOT.TFile(args.inFileName,"READ")
  if inFile.IsZombie():
    print("Error: No input file found with name {}.\nAborting...".format(args.inFileName))
    sys.exit()

  # open output file
  outFile = ROOT.TFile("./outFile.root","RECREATE")
 
  #==================================#
  # Define Dataframes and Selections #
  #==================================#

  # dataframe dictionary for all selections
  df_dict = {}
  
  # define regions
  regList   = ['srl','srs','vrl','vrs','crttbar','bbl','bbs','bxl','bxs','xxl','xxs']

  # import dataframe for each region
  df_reg = {}
  for reg in regList:
    # check if reg is available 
    if inFile.cd(reg) is False:
      continue 
    df = ROOT.RDataFrame(reg+"/outTree", args.inFileName)
    columns = df.GetColumnNames()
    df_reg[reg] = df

  # add inclusive dataframes
  df_dict.update(df_reg)

  # add dataframes split into flavours
  if args.flav:
    print('Splitting into flavours.')
    df_flav = splitFlavours(df_reg)
    if not df_flav:
      print('ERROR: splitFlavours() returned False status.')
      sys.exit()
    else:
      df_dict.update(df_flav)

  # add dataframes split into pT regions
  if args.ptreg:
    print('Splitting into Hcand_pt regions.')
    df_pt = splitPt(df_reg) 
    if not df_pt:
      print('ERROR: splitPt() returned False status.')
      sys.exit()
    else:
      df_dict.update(df_pt)

  # add dataframes split into mass regions
  if args.mreg:
    print('Splitting into Hcand_m regions.')
    df_m = splitMass(df_reg)
    if not df_m:
      print('ERROR: splitMass() returned False status.')
      sys.exit()
    else:
      df_dict.update(df_m)

  # add dataframes with C2 cuts
  if args.c2:
    print('Applying C2 cuts.')
    df_c2 = splitC2(df_reg)
    if not df_c2:
      print('ERROR: splitC2() returned False status.')
      sys.exit()
    else:
      df_dict.update(df_c2)

  #=============================#
  # Define and Write Histograms #
  #=============================#

  # define 1D histogram models
  hModel1D = []
  hModel1D.append( ROOT.RDF.TH1DModel('Hcand_m' ,'',44, 60, 280) )
  hModel1D.append( ROOT.RDF.TH1DModel('Hcand_pt','',95,250,1200) )
  #hModel1D.append( ROOT.RDF.TH1DModel('reweight','',75,  0, 1.5) )
  #hModel1D.append( ROOT.RDF.TH1DModel('Hcand_C2','',50,  0, 0.5) )
  #hModel1D.append( ROOT.RDF.TH1DModel("Hcand_truth_m" ,"",44,60 , 280) )
  #hModel1D.append( ROOT.RDF.TH1DModel("Hcand_truth_pt","",95,250,1200) ) 

  # write 1D histograms
  for key in df_dict:
    for hmod in hModel1D:
      if hmod.fName not in columns:
        print("Column with name {} not found in the dataframe.\nSkipping...".format(hmod.fName))
        continue
      print("Writing 1D histogram: {}_{}".format(key,hmod.fName))
      df  = df_dict[key]
      h1D = []
      if hmod.fName != 'reweight':
        h1D = make1DPlot(df,hmod,'w',args.reweight)
      elif hmod.fName not in columns:
        continue
      else:
        h1D = make1DPlot(df,hmod,'',False)
      outFile.cd()
      h1D.Write('{}_{}'.format(key,hmod.fName))

  # closing input and output files
  inFile.Close()
  outFile.Close()

# execute
if __name__ == '__main__':
  main()
