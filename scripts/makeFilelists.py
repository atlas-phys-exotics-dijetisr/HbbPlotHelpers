#!/usr/bin/env python

import sys
import glob
import os.path

import ROOT

if len(sys.argv)!=1:
    print('usage: {}'.format(sys.argv[0]))
    sys.exit(1)

# Loop over datasets
for ds in glob.glob('user.*ANALYSIS.root'):
    part=ds.split('.')
    camp=part[2]
    isMC=camp.startswith('mc')

    # Get list of sample in dataset
    samples=set(['.'.join(os.path.basename(x).split('.')[2:4]) for x in glob.glob('{}/*root'.format(ds))])

    # Save stuff
    for sample in samples:
        # Loop over all files, calculating information
        fh=open('{}.{}.txt'.format(camp,sample) if isMC else '{}.txt'.format(camp),'w')

        sumN=0
        sumW=0
        syst=None

        for path in glob.glob('{}/*{}*.root'.format(ds,sample)):
            abspath=os.path.abspath(path)
            if abspath.startswith('/eos'): # Stored on EOS
                abspath='root://eosatlas.cern.ch/'+abspath

            fh.write(abspath+'\n')

        fh.close()
